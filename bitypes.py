#!/usr/bin/python
# _*_ coding:utf-8 _*_





def _f(): pass


def _g(): yield 1


class _A(object): pass


biattrs = set(dir(_A()))


types = {'None': set(dir(None)),
         'int': set(dir(int)),
         # 'long': set(dir(long)),
         'float': set(dir(float)),
         'bool': set(dir(bool)),
         'unicode': set(dir(unicode)),
         'str': set(dir(str)),
         'tuple': set(dir(tuple)),
         'list': set(dir(list)),
         'dict': set(dir(dict)),
         'set': set(dir(set)),
         'function': set(dir(_f)),
         # 'lambda': set(dir(lambda x: x)),
         'generator': set(dir(_g())),
         'file': set(dir(file)),
         'iterator': set(dir(iter([]))),
         # 'bytearray': set(dir(bytearray)),
         # 'frozenset': set(dir(frozenset))
         }

param_types = {'abs': {-1: {'types': {'int', 'float'}},
                       0: {'types': {'int', 'float'}}},
               'all': {-1: {'types': {'bool'}},
                       0: {'attrs': {'__iter__'}}},
               'any': {-1: {'types': {'bool'}},
                       0: {'attrs': {'__iter__'}}},
               'apply': {0: {'attrs': {'__code__'}}},
               'bin': {0: {'types': {'int', 'float'}}},
               'chr': {0: {'types': {'int'}}},
               'divmod': {0: {'types': {'int'}},
                          1: {'types': {'int'}}},
               'eval': {0: {'types': {'str'}}},
               'filter': {0: {'attrs': {'__code__'}},
                          1: {'attrs': {'__iter__'}}},
               'getattr': {1: {'types': {'str'}}},
               'hasattr': {1: {'types': {'str'}}},
               'hex': {0: {'types': {'int'}}},
               'intern': {0: {'types': {'str'}}},
               'iter': {0: {'attrs': '__iter__'}},
               'len': {0: {'attrs': '__len__'}},
               'map': {0: {'attrs': {'__code__'}},
                       1: {'attrs': {'__iter__'}}},
               'max': {-1: {'types': {'int', 'float'}},
                       0: {'attrs': {'__iter__'}, 'types': {'int', 'float'}},
                       1: {'types': {'int', 'float'}},
                       2: {'types': {'int', 'float'}},
                       3: {'types': {'int', 'float'}}},
               'min': {-1: {'types': {'int', 'float'}},
                       0: {'attrs': {'__iter__'}, 'types': {'int', 'float'}},
                       1: {'types': {'int', 'float'}},
                       2: {'types': {'int', 'float'}},
                       3: {'types': {'int', 'float'}}},
               'next': {0: {'types': {'iterator'}}},
               'oct': {0: {'types': {'int'}}},
               'open': {-1: {'types': {'file'}},
                        0: {'types': {'str'}}},
               'pow': {-1: {'types': {'int', 'float'}},
                       0: {'types': {'int'}},
                       1: {'types': {'ints'}}},
               'range': {0: {'types': {'int'}},
                         1: {'types': {'int'}},
                         2: {'types': {'int'}}},
               'reduce': {0: {'attrs': {'__code__'}},
                          1: {'attrs': {'__iter__'}}},
               'round': {0: {'types': {'int', 'float'}}},
               'setattr': {1: {'types': {'str'}}},
               'sorted': {0: {'attrs': {'__iter__'}}},
               'sum': {-1: {'types': {'int', 'float'}},
                       0: {'attrs': {'__iter__'}}},
               'zip': {0: {'attrs': {'__iter__'}},
                       1: {'attrs': {'__iter__'}}},
               # =================================
               # int method
               'int': {-1: {'types': {'int'}}},
               # =================================
               # str methods
               'str': {-1: {'types': {'str'}}},
               'center': {0: {'types': {'int'}}},
               'count': {0: {'types': {'str'}}},
               'decode': {-1: {'types': {'str'}},
                          0: {'types': {'str', 'None'}}},
               'encode': {-1: {'types': {'str'}},
                          0: {'types': {'str', 'None'}}},
               'endswith': {-1: {'types': {'bool'}},
                            0: {'types': {'str', 'None'}},
                            1: {'types': {'int', 'None'}},
                            2: {'types': {'int', 'None'}}},
               'expandtabs': {0: {'typesize': {'int', 'None'}}},
               'find': {-1: {'types': {'int'}},
                        0: {'types': {'str'}},
                        1: {'types': {'int', 'None'}},
                        2: {'types': {'int', 'None'}}},
               'index': {-1: {'types': {'int'}},
                         0: {'types': {'str'}},
                         1: {'types': {'int', 'None'}},
                         2: {'types': {'int', 'None'}}},
               'join': {-1: {'types': {'str'}},
                        0: {'attrs': {'__iter__'}}},
               'ljust': {0: {'types': {'int'}}},
               'lstrip': {-1: {'types': {'str'}},
                          0: {'types': {'None', 'str'}}},
               'partition': {0: {'types': {'str'}}},
               'replace': {-1: {'types': {'str'}},
                           0: {'types': {'str'}},
                           1: {'types': {'str'}},
                           2: {'types': {'int', 'None'}}},
               'rfind': {-1: {'types': {'int'}},
                         0: {'types': {'str'}},
                         1: {'types': {'int', 'None'}},
                         2: {'types': {'int', 'None'}}},
               'rindex': {0: {'types': {'str'}},
                          1: {'types': {'int', 'None'}},
                          2: {'types': {'int', 'None'}}},
               'rjust':  {0: {'types': {'int'}},
                          1: {'types': {'str', 'None'}}},
               'rpartition': {0: {'types': {'str'}}},
               'rsplit': {0: {'types': {'str', 'None'}},
                          1: {'types': {'int', 'None'}}},
               'rstip': {0: {'types': {'str', 'None'}}},
               'split': {-1: {'types': {'list'}},
                         0: {'types': {'str', 'None'}},
                         1: {'types': {'int', 'None', 'str'}}},  # we simply merge re.split into this.
               'splitlines': {-1: {'types': {'list'}},
                              0: {'types': {'bool'}}},
               'startswith': {-1: {'types': {'bool'}},
                              0: {'types': {'str', 'None'}},
                              1: {'types': {'int', 'None'}},
                              2: {'types': {'int', 'None'}}},
               'strip': {-1: {'types': {'str'}},
                         0: {'types': {'None', 'str'}}},
               'zfill': {0: {'types': {'int'}}},
               # end =================================
               'enumerate': {0: {'attrs': {'__iter__'}}},
               # ====================================
               # dict methods
               'dict': {-1: {'types': {'dict'}},
                        0: {'attrs': {'__iter__'}}},
               # ====================================
               # list
               'list': {-1: {'types': {'list'}},
                        0: {'attrs': {'__iter__'}}},
               'extend': {0: {'attrs': {'__iter__'}}},
               # end =================================
               'reversed': {0: {'attrs': {'__iter__'}}},
               # ====================================
               # set methods
               'frozenset': {0: {'attrs': {'__iter__'}}},
               'set': {-1: {'types': 'set'},
                       0: {'attrs': {'__iter__'}}},
               'difference': {-1: {'types': {'set'}},
                              0: {'types': {'set'}}},
               'difference_update': {0: {'types': {'set'}}},
               'intersection': {-1: {'types': {'set'}},
                                0: {'types': {'set'}}},
               'isdisjoint': {-1: {'types': {'bool'}},
                              0: {'types': {'set'}}},
               'issubset': {-1: {'types': {'bool'}},
                            0: {'types': {'set'}}},
               'symmetric_difference': {0: {'types': {'set'}}},
               'symmetric_difference_update': {0: {'types': {'set'}}},
               'union': {-1: {'types': {'set'}},
                         0: {'types': {'set'}}},
               'update': {0: {'attrs': {'__iter__'}}},
               # end ================================
               'slice': {0: {'types': {'int', 'None'}},
                         1: {'types': {'int', 'None'}},
                         2: {'types': {'int', 'None'}}},
               # ====================================
               # tuple
               'tuple': {-1: {'types': {'tuple'}},
                         0: {'attrs': {'__iter__'}}},
               # end ================================
               'xrange': {0: {'types': {'int', 'None'}},
                          1: {'types': {'int', 'None'}},
                          2: {'types': {'int', 'None'}}},
               # re module:
               'match': {0: {'types': {'str'}},
                         1: {'types': {'str'}},
                         2: {'types': {'int'}}},
               'search': {0: {'types': {'str'}},
                          1: {'types': {'str'}},
                          2: {'types': {'int'}}},
               'sub': {0: {'types': {'str'}},
                       1: {'types': {'str'}},
                       2: {'types': {'int'}}},
               'findall': {0: {'types': {'str'}},
                           1: {'types': {'str'}},
                           2: {'types': {'int'}}},
               # others:
               'write': {0: {'types': {'str'}}}
               }

builtin_type2subtypes = {'basestring': {'str', 'unicode'}}
