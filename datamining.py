#!/usr/bin/python
# _*_ coding:utf-8 _*_

__author__ = 'root'


import re

import numpy as np
from sklearn.cluster import *
from sklearn.linear_model import *
from sklearn.svm import *
from sklearn.naive_bayes import *
from sklearn.discriminant_analysis import *
from sklearn.tree import *
from sklearn.ensemble import *
from sklearn.neighbors import *
from sklearn.multiclass import *
from sklearn.calibration import CalibratedClassifierCV
from sklearn import metrics
from str_utils import *


def _log(msg):
    print msg


class TypeMinor(object):
    def __init__(self, training_file, testing_file, types_file, k=25):
        self.training_filepath = training_file
        self.testing_filepath = testing_file
        self.types_filepath = types_file
        self.type2id = {}
        self.id2type = {}

        # for storing features
        self.id2cluster = {}
        self.id2posfeature = {}
        self.id2pluarfeature = {}
        self.id_type2hasfeature = {}

        self.id2names = {}
        self.raw_training_data = []
        self.raw_testing_data = []
        self.variables = []
        self.n_clusters = 20
        self.dataset = None
        self.classifier = None
        self.type2classifier = {}
        self.cluster = None
        self.results = []
        self.predicts = {}
        self.history = {}
        self.history_probas = {}
        self.k = k


    def train(self, n_clusters=-1):
        if n_clusters > 0:
            self.n_clusters = n_clusters
        self._init_raw_data()
        self._train_cluster_variables()
        self._extract_dataset()
        # if not self.ovo:
        #     self._train_classify_names()
        # else:
        self._train_classify_names_ovo()

    def predict(self, vname):
        if vname in self.history:
            return self.history[vname]
        test = self._extract_features(vname)
        pred = []
        for tname in self.type2id.iterkeys():
            if tname not in self.type2classifier:
                continue
            if self.type2classifier[tname].predict(np.array([test]))[0]:
                pred.append(tname)
        self.history[vname] = pred
        return pred

    def predict_proba(self, vname):
        if vname in self.history_probas:
            return self.history_probas[vname]
        test = {}
        for tname in self.type2id.iterkeys():
            test[tname] = [self._extract_features(vname, tname)]
        pred = self._inner_predict_proba(test)[0]
        self.history_probas[vname] = pred
        return pred

    def predict_proba_type(self, vname, tname):
        if vname in self.history and tname in self.history[vname]:
            return self.history[vname][tname]
        test = self._extract_features(vname, tname)
        if tname not in self.type2classifier:
            pred = 0.0
        else:
            pred = self.type2classifier[tname].predict(np.array([test]))[0][1]
        if vname not in self.history:
            self.history[vname] = {tname: pred}
        else:
            self.history[vname][tname] = pred
        return pred

    def evaluate_all_tests(self):
        self.evaluate_all_ovo()

    @staticmethod
    def _group_data_by_gap(type_probas, gap):
        elm_wise_data = zip(type_probas, type_probas[1:])
        groups = []
        current_group = [type_probas[0]]
        for (t1, p1), (t2, p2) in elm_wise_data:
            if p1 - p2 <= gap:
                current_group.append((t2, p2))
            else:
                groups.append(current_group)
                current_group = []
        if current_group:
            groups.append(current_group)
        return groups

    def evaluate_all_ovo(self):
        test_data = self._extract_test_dataset()
        predicted_data_proba = self._inner_predict_proba(test_data['data'])
        for i, info in enumerate(self.raw_testing_data):
            #types = {k for k, v in predicted_data_proba[i].iteritems() if v >= 0.5}
            type_probas = sorted(predicted_data_proba[i].items(), key=lambda x: -x[1])
            first_cluster = self._group_data_by_gap(type_probas, 0.1)[0]
            first_cluster_top5 = first_cluster[:min(len(first_cluster), 5)]
            first_cluster_top5 = filter(lambda x: x[1] >= 0.05, first_cluster_top5)
            types = map(lambda x: x[0], first_cluster_top5)
            self.results.append({'id': info['id'], 'context': info['context'],
                                 'location': info['location'], 'dtypes': info['dtypes'],
                                 'stypes': filter(lambda x: x != '?', info['stypes']),
                                 'pred_proba': predicted_data_proba[i], 'ptypes': types})
        # for d in self.results:
        #     print d['id'], sorted(d['pred_proba'].iteritems(), key=lambda x: -x[1])

    # def evaluate_all_mcls(self):
    #     test_data = self._extract_test_dataset()
    #     # predicted_data = self.classifier.predict(test_data['data'])
    #     # predicted_data, predicted_proba = self._predict_proba(test_data['data'])
    #     predicted_data_proba = self._inner_predict_proba(test_data['data'])
    #     # print self.classifier.predict_proba(test_data['data'])
    #     predicted_proba = predicted_data_proba.max(axis=1)
    #     predicted_data = map(lambda i: self.classifier.classes_[i], predicted_data_proba.argmax(axis=1))
    #     for i, info in enumerate(self.raw_testing_data):
    #         pred_proba = {self.id2type[self.classifier.classes_[j]]: proba
    #                       for j, proba in enumerate(predicted_data_proba[i])}
    #         self.results.append({'id': info['id'], 'context': info['context'],
    #                              'location': info['location'], 'dtypes': info['dtypes'],
    #                              'stypes': filter(lambda x: x != '?', info['stypes']),
    #                              'pred_proba': pred_proba, 'ptypes': {self.id2type[predicted_data[i]]}})
    #
    #     # for debugging
    #     count1 = count2 = 0
    #     threshold = 0.5
    #     for i, info in enumerate(self.raw_testing_data):
    #         var, test, pred, proba = info['id'], self.id2type[test_data['targets'][i]], \
    #             self.id2type[predicted_data[i]], predicted_proba[i]
    #         if var not in self.predicts:
    #             self.predicts[var] = (pred, proba)
    #         # print test == pred, '(' + var, test + ')', pred, proba
    #         count1 += (proba >= threshold)
    #         count2 += ((test == pred) and (proba >= threshold))
    #     _log('--------------- summary ----------------')
    #     _log('percentage of correct results with probabilty > {0}: {1:.0f}%'.format(threshold, 1.0*count2/count1*100))
    #     _log('percentage of results with probabilty > {0}: {1:.0f}%'.format(threshold, 1.0*count1/(i+1)*100))
    #     _log("Accuracy: %1.3f" % metrics.accuracy_score(test_data['targets'], predicted_data))
    #     # print("\tPrecision: %1.3f" % metrics.precision_score(test_data['targets'], predicted_data, average="weighted"))
    #     # print("\tRecall: %1.3f" % metrics.recall_score(test_data['targets'], predicted_data, average="weighted"))
    #     # print("\tF1: %1.3f\n" % metrics.f1_score(test_data['targets'], predicted_data, average="weighted"))

    def _inner_predict_proba(self, data):
        # data[type][records]
        proba = {i: {} for i in range(len(data.itervalues().next()))}
        for tname in self.type2id.iterkeys():
            if tname not in self.type2classifier:
                for i in range(len(data[tname])):
                    proba[i][tname] = 0.0
                continue
            classifier = self.type2classifier[tname]
            assert hasattr(classifier, 'predict_proba'), \
                'The classifier does not support propbabilistic prediction'
            for i, (p0, p1) in enumerate(classifier.predict_proba(data[tname])):
                proba[i][tname] = p1
            # proba.append((tname, classifier.predict_proba(data)[0][1]))
        return proba

    @staticmethod
    def _load_raw_data(filepath):
        data = []
        with open(filepath, 'rb') as f:
            for line in f.readlines():
                infos = line.strip().split()
                if not re.match('^_+[\d_]*$', infos[0]):  # we ignore all variables only consists of char '_'
                    data.append({'id': infos[0], 'stypes': infos[1].split('|'),
                                 'dtypes': infos[2].split('|'), 'context': infos[3],
                                 'location': infos[4]})
        return data

    def _init_raw_data(self):
        # init raw data
        self.raw_training_data = self._load_raw_data(self.training_filepath)
        self.raw_testing_data = self._load_raw_data(self.testing_filepath)
        # init all variables
        self.variables = np.array(list(set(map(lambda d: d['id'], self.raw_training_data) +
                                           map(lambda d: d['id'], self.raw_testing_data))))

        # init labels
        types = set()
        for info in self.raw_training_data:
            types.update(info['stypes'])
            types.update(info['dtypes'])
        for info in self.raw_testing_data:
            types.update(info['stypes'])
            types.update(info['dtypes'])
        with open(self.types_filepath) as f:
            lines = f.read().splitlines()
            for t in lines:
                if t.strip():
                    types.add(t.strip())
        if '?' in types:
            types.remove('?')
        # add initial builtin types
        self.type2id = {t: i for i, t in enumerate(types)}

    def _train_cluster_variables(self):
        # variables = np.array(list(set(map(lambda d: d['id'], self.raw_training_data) +
        #                               map(lambda d: d['id'], self.raw_testing_data))))
        variables = self.variables
        distance_matrix = -1 * self._compute_str_distances(variables)
        # clustering = DBSCAN(metric='precomputed', eps=0.8, min_samples=1) # not work
        # clustering = MeanShift()  # not work
        clustering = KMeans(n_clusters=self.k)
        # clustering = AffinityPropagation(affinity='precomputed', damping=0.5)
        # clustering = AgglomerativeClustering(n_clusters=self.n_clusters)
        clustering.fit(distance_matrix)
        self.id2cluster = dict(zip(variables, clustering.labels_))
        self.cluster = clustering

        # debuging the clustering
        # for cluster_id in np.unique(clustering.labels_):
        #     cluster = variables[np.nonzero(clustering.labels_ == cluster_id)]
        #     cluster_str = ", ".join(cluster)
        #     print cluster_str

    def _predict_cluster(self, vname):
        distance = -1 * self._compute_str_distances([vname])
        return self.cluster.predict(distance)[0]

    def _compute_str_distances(self, data):
        _extract_str_distance = self._extract_str_distance
        return np.array([[_extract_str_distance(name1, name2) for name2 in self.variables]
                         for name1 in data])

    def _extract_str_distance(self, var1, var2):
        names1, names2 = self._split_var(var1), self._split_var(var2)
        dis = (self._compute_substr_distance(names1, names2)
               + self._compute_abbrvstr_distance(names1, names2)
               # + self._compute_pos_distance(names1, names2)
               # + self._compute_sp_distance(names1, names2)
               )
        return dis

    @staticmethod
    def _compute_substr_distance(names1, names2):
        name1 = ''.join(map(str.lower, names1))
        name2 = ''.join(map(str.lower, names2))
        min_len = min(len(name1), len(name2))
        substr = list(find_longest_common_str(name1, name2))
        return 1 - (len(substr[0]) if substr else 0) * 1.0 / min_len

    @staticmethod
    def _compute_abbrvstr_distance(names1, names2):
        names1, names2 = map(str.lower, names1), map(str.lower, names2)
        name1, name2 = ''.join(names1), ''.join(names2)
        if len(name1) > len(name2):
            name1, names2 = (name2, names1)
        return 1 - is_abbrev_for_multiple(name1, names2)

    @staticmethod
    def _compute_pos_distance(names1, names2):
        tags1 = {t for _, t in get_pos(' '.join(names1))}
        tags2 = {t for _, t in get_pos(' '.join(names2))}
        return int(tags1 != tags2)

    @staticmethod
    def _compute_sp_distance(names1, names2):
        return 1 - int(is_singluar(names1[-1]) == is_singluar(names2[-1]))

    def _split_var(self, name):
        if name in self.id2names:
            return self.id2names[name]
        final_names = []
        # try to remove tail digits first
        name = self._remove_tail_digits(name)
        names = map(self._remove_tail_digits, name.split('_'))
        for name in names:
            if self._check_if_all_capitals(name):
                final_names.append(name)
            else:
                subnames = self._split_by_captials(name)
                subnames = map(self._remove_tail_digits, subnames)
                subnames = map(str.lower, subnames)
                final_names.extend(subnames)
        final_names = filter(bool, final_names)
        self.id2names[name] = final_names
        return final_names

    @staticmethod
    def _remove_tail_digits(name):
        return name.rstrip('1234567890')

    @staticmethod
    def _check_if_all_capitals(name):
        return name.upper() == name

    @staticmethod
    def _split_by_captials(name):
        return filter(bool, re.split('([A-Z][^A-Z]*)', name))

    # def _train_classify_names(self, data, targets):
    #     # self.classifier = LinearSVC()
    #     self.classifier = SVC(probability=True)  # B-C-D-E not stable
    #     # self.classifier = MultinomialNB()  # D
    #     # self.classifier = BernoulliNB()  # E
    #     # self.classifier = GaussianNB()  # C  # proba
    #     # self.classifier = LinearDiscriminantAnalysis()  # C-D not stable
    #     # self.classifier = DecisionTreeClassifier()  # C-D not stable
    #     # self.classifier = DecisionTreeRegressor() # not work
    #     # self.classifier = BaggingClassifier(KNeighborsClassifier(),  # B-C-D super not stable
    #     #                                     max_samples=0.5, max_features=0.5)
    #     # self.classifier = RandomForestClassifier(n_estimators=20)  # C-D
    #     # self.classifier = ExtraTreesClassifier(n_estimators=10, max_depth=None,
    #     #                                        min_samples_split=1, random_state=0)  # D
    #     # self.classifier = OneVsRestClassifier(LinearSVC(random_state=0))  # B-C
    #     # self.classifier = OneVsOneClassifier(LinearSVC(random_state=0))  # A-B  not stable
    #     # self.classifier = OneVsOneClassifier(SVC(probability=True, random_state=0))
    #     # self.classifier = OneVsRestClassifier(SVC(probability=True, random_state=0))  # A-B  not stable
    #     # self.classifier = OutputCodeClassifier(LinearSVC(random_state=0),
    #     #                                        code_size=2, random_state=0) # A-B
    #     # self.classifier = LogisticRegression(multi_class='multinomial', C=150, solver='lbfgs')
    #     # self.classifier = CalibratedClassifierCV(SVC(), cv=1, method='isotonic')
    #     self.classifier.fit(data, targets)

    def _train_classify_names_ovo(self):
        for tname, id in self.type2id.iteritems():
            targets = map(lambda x: int(x == id), self.dataset['targets'])
            if not any(targets):
                continue
            # classifier = LogisticRegression(multi_class='multinomial', C=150, solver='lbfgs')
            classifier = SVC(probability=True)
            classifier.fit(self.dataset['data'][tname], targets)
            self.type2classifier[tname] = classifier

    # def _extract_dataset(self):
    #     data, targets = [], []
    #     for info in self.raw_training_data:
    #         data.append(self._extract_features(info))
    #         targets.append(self._extract_target(info))
    #     id2label = {v: k for k, v in self.type2id.iteritems()}
    #     self.dataset = {'data': np.array(data), 'targets': np.array(targets)}
    #     self.id2type = id2label

    def _extract_dataset(self):
        data = {tname: [] for tname in self.type2id.iterkeys()}
        targets = []
        for info in self.raw_training_data:
            for tname in self.type2id.iterkeys():
                data[tname].append(self._extract_features(info, tname))
            targets.append(self._extract_target(info))

        id2label = {v: k for k, v in self.type2id.iteritems()}
        self.dataset = {'data': {tname: np.array(d) for tname, d in data.iteritems()},
                        'targets': targets}
        self.id2type = id2label

    def _extract_test_dataset(self):
        data = {tname: [] for tname in self.type2id.iterkeys()}
        targets = []
        for info in self.raw_testing_data:
            for tname in self.type2id.iterkeys():
                data[tname].append(self._extract_features(info, tname))
            targets.append(self._extract_target(info))
        return {'data': {tname: np.array(d) for tname, d in data.iteritems()},
                'targets': targets}

    # def _extract_test_dataset(self):
    #     data, targets = [], []
    #     for info in self.raw_testing_data:
    #         data.append(self._extract_features(info))
    #         targets.append(self._extract_target(info))
    #     return {'data': np.array(data), 'targets': np.array(targets)}

    def _extract_features(self, info, tname):
        vname = info['id'] if isinstance(info, dict) else str(info)
        if vname in self.id2cluster:
            id_lit_f = self.id2cluster[vname]
        else:
            id_lit_f = self._predict_cluster(vname)

        if vname not in self.id2posfeature:
            id_pos_f = self._extract_pos_feature(vname)
            self.id2posfeature[vname] = id_pos_f
        else:
            id_pos_f = self.id2posfeature[vname]

        if vname not in self.id2pluarfeature:
            id_sp_f = self._extract_sp_feature(vname)
            self.id2pluarfeature[vname] = id_sp_f
        else:
            id_sp_f = self.id2pluarfeature[vname]

        if (vname, tname) not in self.id_type2hasfeature:
            has_f = self._extract_has_feature(vname, tname)
            self.id_type2hasfeature[(vname, tname)] = has_f
        else:
            has_f = self.id_type2hasfeature[(vname, tname)]

        return [id_lit_f, id_pos_f, id_sp_f, has_f]

    def _extract_target(self, info):
        return self.type2id[list(info['dtypes'])[0]]

    def _extract_lit_features(self, var):
        _extract_str_distance = self._extract_str_distance
        return [_extract_str_distance(var, name2) for name2 in self.variables]

    def _extract_pos_feature(self, var):
        names = self._split_var(var)
        tags = [t for _, t in get_pos(' '.join(names))]
        for t in tags:
            if t == wn.VERB:
                return 1
            if t == wn.NOUN:
                return 0
        else:
            return 2

    def _extract_sp_feature(self, var):
        names = self._split_var(var)
        return int(is_singluar(names[-1]))

    def _extract_cnt_feature(self, cnt):
        return 1

    def _extract_loc_feature(self, loc):
        return 1

    def _extract_has_feature(self, var, tname):
        return self._extract_str_distance(var, tname)

    def make_summary(self):
        recall = 0.0
        for data in self.results:
            recall += 1.0 * len(set(data['dtypes']).intersection(set(data['ptypes']))) / len(data['dtypes'])
        recall /= len(self.results)
        _log('--------mining results---------')
        _log('[Gap:0.1][Top5]\nRecall:{:.2%}'.format(recall))
        

if __name__ == '__main__':

    def _format_float(x):
        return float('%.4f' % x)

    base_dir = '/home/gui/Current/NamingProject/ServerResults/MData/'
    proj = 'httptools/httpie/'
    filepath1 = base_dir + proj + 'same.txt'
    filepath2 = base_dir + proj + 'diff.txt'
    filepath3 = base_dir + proj + 'stypes.txt'
    recall, precision = 0.0, 0.0
    for i in range(10):
        print 'Time: ' + str(i)
        predictor = TypeMinor(filepath1, filepath2, filepath3)
        predictor.train()
        predictor.evaluate_all_tests()
        r, p = predictor.make_summary()
        recall += r
        precision += p
    print '-----------------------'
    print 'AVG:', _format_float(1.0 * recall / 10), _format_float(1.0 * precision / 10)
    # print sorted(predictor.predict_proba('headers').iteritems(), key=lambda x: -x[1])
    # print predictor.predict('str_m')
    # for k, v in predictor.predict_proba('str_3').iteritems():
    #     print k, ":", v
    # for re in predictor.results:
    #     print re['ptypes'].issubset(re['dtypes']), re['id'], re['ptypes'], re['dtypes'], \
    #         re['pred_proba'][list(re['ptypes'])[0]], re['pred_proba'].get(list(re['dtypes'])[0])
    # sorted([(k, v) for k, v in re['pred_proba'].iteritems()], key=lambda x: -x[1])
    # print predictor._extract_pos_feature('something')
    # print _compute_sp_distance(['faces'], ['ask'])
    # predictor._extract_str_distance('ImmutableMultiDict', 'multi2')
