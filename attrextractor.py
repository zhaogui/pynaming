#!/usr/bin/python
# _*_ coding:utf-8 _*_


class AttrExtractor(object):

    def visit(self, node):
        if not node: return ()
        nodename = node.__class__.__name__.lower()
        visit_func = getattr(self, 'visit_' + nodename, self.default_visit)
        return visit_func(node)

    def default_visit(self, *_):
        pass
