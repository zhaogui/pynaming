#!/usr/bin/python
# _*_ coding:utf-8 _*_

import re
import sys
import types
import pkgutil
import inspect
import importlib
from os import makedirs
from os.path import exists, join, sep, splitext
from utils import get_filepaths, relative_path
from dataextractor import extract_all as extract_dynamic_data_to_one_file


def _log(msg, level='info'):
    if level == 'info':
        print >>sys.stdout, msg
    else:
        print >>sys.stderr, msg


class InfoPkg(object):
    def __init__(self, id, types, context, location):
        self.id = id
        self.types = types
        self.context = context
        self.location = location

    def __str__(self):
        return '{id} {types} {context} {location}'.format(id=self.id, types='|'.join(self.types),
                                                          context=self.context, location=self.location)


class MergedInfoPkg(object):
    def __init__(self, id, stypes, dtypes, context, location):
        self.id = id
        self.stypes = stypes
        self.dtypes = dtypes
        self.context = context
        self.location = location

    def __str__(self):
        return '{id} {stype} {dtype} ' \
               '{context} {location}'.format(id=self.id,
                                             stype='|'.join(self.stypes),
                                             dtype='|'.join(self.dtypes),
                                             context=self.context,
                                             location=self.location)


class DataMerger(object):
    def __init__(self, projname, projroot, dynamic_data_dir, static_data_dir, output_dir,
                 dynamic_data_contains='', dynamic_data_ignores='',
                 merging_ignored_dirs=(), merging_ignored_types=(),
                 merging_changed_types=(), merging_changed_data=(),
                 dynamic_loading_dis=2):
        self.projname = projname
        self.projroot = projroot
        self.dynamic_data_dir = dynamic_data_dir
        self.static_data_dir = static_data_dir
        self.output_dir = output_dir
        self.dynamic_data_contains = dynamic_data_contains
        self.dynamic_data_ignores = dynamic_data_ignores

        self.merging_ignored_dirs = merging_ignored_dirs
        self.merging_ignored_types = merging_ignored_types
        self.merging_changed_types = {} if not merging_changed_types else merging_changed_types
        self.merging_changed_data = {} if not merging_changed_data else merging_changed_data

        self.dynamic_loading_dis = dynamic_loading_dis

        if not exists(self.output_dir):
            makedirs(self.output_dir)
        
        self.log_dir = join('log', self.projname)
        if not exists(self.log_dir):
            makedirs(self.log_dir)

    def _load_dynamic_data(self):
        ddata = extract_dynamic_data_to_one_file(pkldir=join(self.dynamic_data_dir, 'pkls'),
                                                 contains=self.dynamic_data_contains,
                                                 ignores=self.dynamic_data_ignores,
                                                 outfile=join(self.dynamic_data_dir, 'extracted-data.txt'))
        # preprocess data
        indexed_ddata = {}
        for info in ddata:
            dtypes = map(lambda t: t.split('.')[-1], info.types)
            location = relative_path(self.projroot, info.location)
            indexed_ddata[info.id + '-' + location] = InfoPkg(info.id, dtypes, info.context, location)
        return indexed_ddata

    def _load_static_data(self):
        sdata = {}
        allfiles = get_filepaths(self.static_data_dir)
        for filepath in allfiles:
            if filepath.endswith('.py.txt'):
                rpath = splitext(relative_path(self.static_data_dir, filepath))[0]
                with open(filepath, 'rb') as f:
                    data = {}
                    source = f.read()
                    lines = source.splitlines()
                    for line in lines:
                        if line:
                            id, loc, types = re.findall('<<(.*?)>>', line)[0:3]
                            if types.startswith('{'):
                                types = types[1:-1]
                            location = rpath + '#' + loc
                            types = set(filter(bool, types.split('||')))
                            infopkg = InfoPkg(id, types, None, location)
                            data[id + '-' + location] = infopkg
                    sdata.update(data)
        return sdata

    def merge_data(self):
        _log('>>>Loading static and dynamic data...')
        ddata = self._load_dynamic_data()
        sdata = self._load_static_data()
        # first, ignore those specified dirs
        if self.merging_ignored_dirs:
            def filter_func(loc):
                for d in self.merging_ignored_dirs:
                    if d in loc:
                        return False
                return True

            ddata = {k: v for k, v in ddata.iteritems() if filter_func(k)}
            sdata = {k: v for k, v in sdata.iteritems() if filter_func(k)}
            
        # _log('Number of static data before denoising: {}'.format(len(sdata)))
        # _log('Number of dynamic data before denoising: {}'.format(len(ddata)))
        
        # second, denoise data we dont care.

        ignored_data_if_having_ids = {'self', 'cls'}
        ignored_data_if_having_types = {'type', 'module', 'ABCMeta'}
        ignored_types = set(self.merging_ignored_types)
        changed_types = self.merging_changed_types

        def _filter_data(noisy_data, data_type):
            ndata = {}
            for k, info in noisy_data.iteritems():
                # filter special ids
                if info.id in ignored_data_if_having_ids:
                    continue
                # change some special types
                info.types = _change_types(info.types)
                # filter special types
                if not set(info.types).isdisjoint(ignored_data_if_having_types):
                    continue
                info.types = _ignore_types(info.types)
                if not info.types:
                    continue
                if data_type == 'dynamic':
                    if info.location in self.merging_changed_data:
                        info.types = self.merging_changed_data[info.location]
                ndata[k] = info
            return ndata

        def _ignore_types(ts):
            return set(ts) - ignored_types

        def _change_types(ts):
            return {(changed_types[t] if t in changed_types else t) for t in ts}
        
        _log('>>>Making statistics for dynamic and static data...')
        self._dump_raw_diff_same_data(sdata, ddata)
        
        den_sdata = _filter_data(sdata, 'static')
        den_ddata = _filter_data(ddata, 'dynamic')
        # _log('Number of static data after denoising: {}'.format(len(den_sdata)))
        # _log('Number of dynamic data after denoising: {}'.format(len(den_ddata)))

        _log('>>>Dumping static data to output file...')
        self._dump_static_data(den_sdata.itervalues())

        _log('>>>Collecting types from static data...')
        alltypes_appeared_in_sdata = self._collect_statically_inferred_types(den_sdata.itervalues())
        # _log('Number of types extracted from static data: {}'.format(len(alltypes_appeared_in_sdata)))

        _log('>>>Dumping types...')
        self._dump_statically_collected_types(alltypes_appeared_in_sdata)

        _log('>>>Enhancing type domain by blindly loading...')
        dynamic_type2attrs, type2subtypes = self._collect_dynamically_loading_types()

        new_dynamic_type2attrs = {}
        for t, attrs in dynamic_type2attrs.iteritems():
            if t in ignored_types:
                continue
            if t in changed_types:
                new_t = changed_types[t]
            else:
                new_t = t
            if new_t in new_dynamic_type2attrs:
                new_dynamic_type2attrs[new_t].update(attrs)
            else:
                new_dynamic_type2attrs[new_t] = set(attrs)

        new_type2subtypes = {}
        for t, subtypes in type2subtypes.iteritems():
            if t in ignored_types:
                continue
            if t in changed_types:
                new_t = changed_types[t]
            else:
                new_t = t
            if new_t in new_type2subtypes:
                new_type2subtypes[new_t].update(subtypes)
            else:
                new_type2subtypes[new_t] = set(subtypes)

        _log('>>>Dumping dynamic types...')
        self._dump_dynamically_loading_types(new_dynamic_type2attrs, new_type2subtypes)

        _log('>>>Merging data...')
        mdata, missdata, nicemiss_data = self._merge_data(den_sdata, den_ddata)
        diff_mdata, same_mdata = self._separate_mdata(mdata)
        # _log('Number of different data after denosing: {}'.format(len(diff_mdata)))
        # _log('Number of same data: {} after denosing: {}'.format(len(same_mdata)))
        # _log('Number of items of missing items: {}'.format(len(missdata)))
        # _log('Number of items of nice missing items: {}'.format(len(nicemiss_data)))

        _log('>>>Dumping merged data to output file...')
        self._dump_merged_data(diff_mdata, same_mdata, missdata, nicemiss_data)
        _log('Finished!')

    @staticmethod
    def _collect_statically_inferred_types(sdata):
        alltypes = set()
        for info in sdata:
            alltypes.update(set(info.types))
        if '?'in alltypes:
            alltypes.remove('?')
        return alltypes

    def _collect_dynamically_loading_types(self):
        if self.dynamic_loading_dis <= 0:
            return {}, {}
        all_types = {}
        type2subtypes = {}
        all_modules = set()
        pkgs = [join(self.projroot, self.projname)]

        # collect all the related modules in the project
        sys.path.insert(0, self.projroot)
        for loader, mname, is_pkg in pkgutil.walk_packages(path=pkgs, prefix=self.projname+'.'):
            try:
                #old_stdout, old_stderr = sys.stdout, sys.stderr
                #sys.stdout = sys.stderr = None  # keep silent
                # _log('begining loading module: <' + name + '>')
                # module = loader.find_module(name).load_module(name)
                module = importlib.import_module(mname)
                # print module.__file__
                if any([True for d in self.merging_ignored_dirs if d in module.__file__]):
                    continue
                all_modules.add(module)
            except (Exception, SystemExit):
                # keep silent
                #import traceback
                #print traceback.format_exc()
                #_log('>> loading module <{}> failed!'.format(mname))
                # print '=============================='
                pass
            # finally:
            #     #sys.stdout, sys.stderr = old_stdout, old_stderr
            #     pass
        sys.path.pop(0)

        # transitively collect all the modules
        _log('Transitively collect all the loaded modules...')
        worklist = [(m, 0) for m in all_modules]
        processed = set()
        while worklist:
            module, dis = worklist.pop(0)
            processed.add(module)
            for name, value in inspect.getmembers(module):
                if name.startswith('__') or name == 'builtins':
                        continue
                if isinstance(value, types.ModuleType):
                    if dis < (self.dynamic_loading_dis - 1) and (value not in processed):
                        worklist.append((value, dis + 1))
        _log('Collecting all types and hierarchies...')
        for module in processed:
            for name, value in inspect.getmembers(module):
                if name.startswith('__'):
                    continue
                if isinstance(value, (types.ClassType, types.TypeType)):
                    if value.__name__ in all_types:
                        all_types[value.__name__].update(dir(value))
                    else:
                        all_types[value.__name__] = set(dir(value))
                    self._collect_type_hierarchy(value, type2subtypes)
        _log('Successfully loaded {} modules and {} types'.format(len(processed), len(all_types)))
        return all_types, type2subtypes

    def _collect_type_hierarchy(self, tvalue, type2subtypes):
        bases = getattr(tvalue, '__bases__', ())
        if (not bases) or (len(bases) == 1 and bases[0].__name__ == 'object'):
            return
        for base in bases:
            if not base.__name__:
                continue
            if base.__name__ in type2subtypes:
                type2subtypes[base.__name__].add(tvalue.__name__)
            else:
                type2subtypes[base.__name__] = {tvalue.__name__}
            self._collect_type_hierarchy(base, type2subtypes)

    @staticmethod
    def _merge_data(sdata, ddata):
        mdata, missing, nicemissing = [], [], []
        import __builtin__

        def _should_recover_missing(id, dtypes):
            if hasattr(__builtin__, id):
                return False
            if not set(dtypes).isdisjoint({'dict', 'list', 'set',
                                           'tuple', 'int', 'str',
                                           'bool'}):
                return True
            else:
                return False

        # merge data based on the dynamic types
        for loc, infopkg in ddata.iteritems():
            if loc in sdata:
                minfopkg = MergedInfoPkg(id=infopkg.id, stypes=set(sdata[loc].types),
                                         dtypes=infopkg.types, context=infopkg.context,
                                         location=infopkg.location)
                mdata.append(minfopkg)
            else:
                minfopkg = MergedInfoPkg(id=infopkg.id, stypes={'?'},
                                         dtypes=infopkg.types, context=infopkg.context,
                                         location=infopkg.location)
                missing.append(minfopkg)
                if _should_recover_missing(minfopkg.id, minfopkg.dtypes):
                    # _log('recover a new item: [' + str(infopkg) + ']')
                    mdata.append(minfopkg)
                if not hasattr(__builtin__, minfopkg.id):
                    nicemissing.append(minfopkg)
        return mdata, missing, nicemissing

    @staticmethod
    def _separate_mdata(mdata):
        diff, same = [], []
        for info in mdata:
            if '?' in info.stypes:
            # if not set(info.stypes).issuperset(set(info.dtypes)):
                diff.append(info)
            else:
                same.append(info)
        return diff, same

    @staticmethod
    def _separate_mdata2(mdata):
        diff, same = [], []
        for info in mdata:
            if '?' in info.stypes or not set(info.stypes).issuperset(set(info.dtypes)):
                diff.append(info)
            else:
                same.append(info)
        return diff, same
    
    def _dump_raw_diff_same_data(self, sdata, ddata):
        mdata, missdata, nicemiss_data = self._merge_data(sdata, ddata)
        diff_mdata, same_mdata = self._separate_mdata2(mdata)
        diff_len, same_len = len(diff_mdata), len(same_mdata)
        failed_percent = (1.0 * diff_len) / (diff_len + same_len)
        
        from pynaming.logger import Tee
        raw_diff_same_output = join(self.log_dir, 'raw-diff-same-summaries.txt')
        _log('-------raw diff-same summary-------')
        with Tee(raw_diff_same_output, 'w'):
            _log('Number of raw different data: {}'.format(diff_len))
            _log('Number of raw same data: {}'.format(same_len))
            _log('Percentage of raw different data: {:.2%}'.format(failed_percent))
                
    def _dump_static_data(self, sdata):
        static_output = join(self.output_dir, 'sdata.txt')
        with open(static_output, 'w') as f5:
            i = 0
            for d in sdata:
                if d.types and ('?' not in d.types) and\
                        (len(d.types) == 1):
                    i += 1
                    print >>f5, str(d)
        
        _log('{} items of static data were dumped'.format(i))
        
    def _dump_statically_collected_types(self, types):
        types_output = join(self.output_dir, 'stypes.txt')
        with open(types_output, 'w') as f:
            for t in types:
                print >>f, str(t)
        
        _log('{} static collected types were dumped.'.format(len(types)))

    def _dump_dynamically_loading_types(self, dynamic_types, type2subtypes):
        types_output = join(self.output_dir, 'dtypes.txt')
        with open(types_output, 'w') as f:
            for t, attrs in dynamic_types.iteritems():
                print >>f, str(t), '|'.join(attrs)
        _log('{} types were dumped.'.format(len(dynamic_types)))

        type2subtypes_output = join(self.output_dir, 'dsubtypes.txt')
        with open(type2subtypes_output, 'w') as f:
            for b, types in type2subtypes.iteritems():
                print >>f, b, '|'.join(types)
        _log('{} items of subtyping relations were dumped.'.format(len(type2subtypes)))
        
    def _dump_merged_data(self, diffdata, samedata, missing, nicemissing):
        same_output = join(self.output_dir, 'same.txt')
        diff_output = join(self.output_dir, 'diff.txt')
        # This is just for debugging.
        
        # miss_output = join(self.output_dir, 'miss.txt')
        # nice_miss_output = join(self.output_dir, 'nicemiss.txt')

        with open(diff_output, 'w') as f1:
            for d in diffdata:
                print >>f1, str(d)
        
        _log('{} items of diff-data (after denosing) were dumped'.format(len(diffdata)))

        with open(same_output, 'w') as f2:
            for d in samedata:
                print >>f2, str(d)
        
        _log('{} items of same-data (after denosing) were dumped'.format(len(samedata)))

        # with open(miss_output, 'w') as f3:
        #     for d in missing:
        #         print >>f3, str(d)

        # with open(nice_miss_output, 'w') as f4:
        #     for d in nicemissing:
        #         print >>f4, str(d)

    

    # def _adjust_data(self, noisy_data):
    #     ignored_types = set(self.merging_ignored_types)
    #     changed_types = self.merging_changed_types
    #     changed_data = self.merging_changed_data
    #     for data in noisy_data:
    #         data.dtypes = set(data.dtypes) - ignored_types
    #         data.dtypes = {changed_types[t] if t in changed_types else t for t in data.dtypes}
    #         if data.location in changed_data:
    #             data.dtypes = set(changed_data[data.location])
    #     return filter(lambda x: x.dtypes, noisy_data)


