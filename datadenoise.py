#!/usr/bin/python
# _*_ coding:utf-8 _*_

__author__ = 'root'


def denoise(data):
    # data = remove_self_data(data)
    # data = remove_type_data(data)
    # data = remove_module_data(data)
    # replace_unicode_with_str(data)
    # return data
    ndata = []
    for info in data:
        # filter special ids
        if info.id in ('self', 'cls'):
            continue
        types = _get_types(info)
        # filter special types
        if 'type' in types:
            continue
        elif 'module' in types:
            continue
        elif 'ABCMeta' in types:
            continue
        # replace some special types
        if 'unicode' in types:
            types.remove('unicode')
            types.add('str')
        ndata.append(info)
    return ndata


def _get_types(info):
    if hasattr(info, 'dtypes'):
        types = info.dtypes
    else:
        types = info.types
    return types



