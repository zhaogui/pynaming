#!/usr/bin/python
# _*_ coding:utf-8 _*_

import sys
import os
import pkgutil
import inspect
import types
import importlib
from os.path import join


def _log(msg):
    print msg


def collect_types(projroot, projname):
    all_types = set()
    sys.path.insert(0, projroot)
    pkgs = [join(projroot, projname)]
    print pkgs
    for loader, name, is_pkg in pkgutil.walk_packages(path=pkgs, prefix='requests.'):
        try:
            _log('begining loading module: <' + name + '>')
            # module = loader.find_module(name).load_module(name)
            # module = __import__(name)
            module = importlib.import_module(name)
            for name, value in inspect.getmembers(module):
                if name.startswith('__'):
                    continue
                if isinstance(value, (types.ClassType, types.TypeType)):
                    all_types.add(value)
                    print value.__name__
            _log('succeeded!')
        except:
            _log('>> failed!')
    print len(all_types), all_types

# def try_load(pyfile):
#     for loader, name, is_pkg in pkgutil.walk_packages(__path__):
#         module = loader.find_module(name).load_module(name)
#
#         for name, value in inspect.getmembers(module):
#             if name.startswith('__'):
#                 continue


if __name__ == '__main__':
    collect_types('/home/gui/Current/NamingProject/Benchmarks/httptools/requests', 'requests')