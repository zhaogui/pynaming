#!/usr/bin/python
# _*_ coding:utf-8 _*_

import re
from ast import *
from itertools import groupby
from dump_python import parse_file
from os.path import sep, join
from multiprocessing import TimeoutError
from multiprocessing.dummy import Pool as ThreadPool


__all__ = ['cluster_refs', 'get_scope', 'FunctionVisitor']


ast_cache = {}


# def cluster_refs(projroot, binding):
    # scope = get_scope(projroot, binding)
    # now we only consider intraproc sensitive
    # if not isinstance(scope, FunctionDef):
        # yield binding.refs
    # else:
        # visitor = FunctionVisitor(binding)
        # visitor.visit_scope(scope)
        # for refs, _ in groupby(sorted(visitor.groups)):
            # if refs:
                # yield refs
                # 


def async_cluster_refs(projroot, binding):
    scope = get_scope(projroot, binding)
    # now we only consider intraproc sensitive
    if not isinstance(scope, FunctionDef):
        yield binding.refs
    else:
        p = ThreadPool(1)
        res = p.apply_async(_cluster_refs, args=(scope, binding))
        try:
            out = res.get(timeout=60)
            for refs, _ in groupby(out):
                if refs:
                    yield refs
        except TimeoutError:
            print 'Abort cluster refs of <%s> for timeout.' % str(binding)
            p.terminate()
            yield binding.refs


def cluster_refs(projroot, binding):
    scope = get_scope(projroot, binding)
    # now we only consider intraproc sensitive
    if not isinstance(scope, FunctionDef):
        yield binding.refs
    else:
        for refs, _ in groupby(_cluster_refs(scope, binding)):
            if refs:
                yield refs
            

def _cluster_refs(scope, binding):
    visitor = FunctionVisitor(binding)
    visitor.visit_scope(scope)
    return sorted(visitor.groups)


def get_scope(projroot, binding):
    filepath, lno, start, end = re.findall(r'(.*?)#(\d+):(\d+)-(\d+)$', binding.loc)[0]
    lno, start, end = int(lno), long(start), long(end)
    if not filepath.startswith(sep):
        filepath = join(projroot, filepath)
    if filepath not in ast_cache:
        ast_tree = parse_file(filepath)
        ast_cache[filepath] = ast_tree
    else:
        ast_tree = ast_cache[filepath]
    scopes = (n for n in walk(ast_tree) if isinstance(n, (ClassDef, FunctionDef, Lambda))
              and getattr(n, 'start', -1) <= start and getattr(n, 'end', -1) >= end)
    # scopes = []
    # for n in walk(ast_tree):
    #     if isinstance(n, (ClassDef, FunctionDef, Lambda)):
    #         print getattr(n, 'name', ''), n.start, n.end
    #         print type(n.start)
    #         if n.start <= start and n.end >= end:
    #             scopes.append(n)

    scopes = sorted(scopes, key=lambda x: -x.start)

    # print scope.lineno, scope.start, scope.end
    # print lno, start, end

    return scopes[0] if scopes else ast_tree

    # return scope


class FunctionVisitor(object):
    def __init__(self, binding):
        self.groups = [[]]
        self.name = binding.name
        self.refs = self._init_refs(binding.refs)

    @staticmethod
    def _init_refs(refs):
        pattern = r'#(\d+):(\d+)-(\d+)$'  # (lno, start, end)
        return {tuple(map(int, re.findall(pattern, ref.loc)[0])): ref for ref in refs}

    def visit(self, node):
        """Visit a node."""
        method = 'visit_' + str.lower(node.__class__.__name__)
        visitor = getattr(self, method, self.default_visit)
        visitor(node)
        # return visitor(node)

    def default_visit(self, node):
        # Fix: If we visit nested scope, we may meet efficiency issue.
        # test case: definition request@web2py/gluon/sqlhtml#2062
        # We simply pass visiting nested functiondef and classdef
        if not isinstance(node, (FunctionDef, ClassDef)):
            self.visit_simple(node)

    def visit_simple(self, node):
        founds = self._search_refs(node)
        if founds:
            new_groups = []
            for group in self.groups:
                new_groups.append(group + list(founds))
            self.groups = new_groups

    def visit_scope(self, node):
        if isinstance(node, FunctionDef):
            self.visit_simple(node.args)
            for s in node.body:
                self.visit(s)

    def visit_if(self, node):
        self.visit_simple(node.test)
        self.visit_branches(node.body, node.orelse)

    def visit_for(self, node):
        self.visit_simple(node.iter)
        self.visit_branches(node.body, node.orelse)

    def visit_while(self, node):
        self.visit_simple(node.test)
        self.visit_branches(node.body, node.orelse)

    def visit_with(self, node):
        self.visit_simple(node.context_expr)
        for s in node.body:
            self.visit(s)

    def visit_tryexcept(self, node):
        self.visit_branches(node.body, node.orelse)

    def visit_tryfinally(self, node):
        for s in node.body + node.finalbody:
            self.visit(s)

    def visit_branches(self, body, orelse):
        groups1 = groups2 = self.groups
        if self._pre_search_refs(*body):
            groups1 = self.visit_block(body)
        if self._pre_search_refs(*orelse):
            groups2 = self.visit_block(orelse)
        if self.groups is not groups1 or self.groups is not groups2:
            self.groups = groups1 + groups2

    def visit_block(self, stmts):
        groups = list(self.groups)
        for stmt in stmts:
            self.visit(stmt)
        self.groups, groups = groups, self.groups
        return groups

    def _pre_search_refs(self, *nodes):
        for node in nodes:
            for n in walk(node):
                if isinstance(n, Name) and n.id == self.name:
                    key = (n.lineno, n.start, n.end)
                    if key in self.refs:
                        return True
        return False

    def _search_refs(self, node):
        founds = []
        if not node: return founds
        for n in walk(node):
            if isinstance(n, Name) and n.id == self.name:
                key = (n.lineno, n.start, n.end)
                if key in self.refs:
                    founds.append(self.refs[key])
                    del self.refs[key]
        return founds


if __name__ == '__main__':
    from analyzer import Reference, Binding

    class TestDriver(object):
        def __init__(self, projroot, binding_file, ref_file):
            self.projroot = projroot
            self.binding_file = binding_file
            self.ref_file = ref_file
            self.binding_table = {}
            self.ref_table = {}
            self.loc2gid = {}

        def extract_raw_data(self):
            self._extract_all_bindings()
            self._extract_all_refs()

        def test(self):
            self.extract_raw_data()
            binds = sorted(self.binding_table.itervalues(), key=lambda x: -len(x.refs))
            for b in binds[1:2]:
                print b
                for i, group in enumerate(cluster_refs(self.projroot, b)):
                    print i
                    print '[' + ',\n'.join(str(ref) for ref in group) + ']'
                    print '----------------------------'

        def _extract_all_bindings(self):
            # extract all the bindings
            with open(self.binding_file) as bf:
                lines = filter(bool, bf.read().split('\n'))
                for line in lines:
                    gid = int(re.findall(r'\[(.*?)\]', line)[0])
                    name, loc = re.findall(r'<(.*?)>', line)[:2]
                    if loc.startswith(self.projroot):
                        loc = loc[len(self.projroot):]
                        in_proj = True
                    else:
                        in_proj = False
                    self.binding_table[gid] = Binding(gid, name, loc, in_proj)
                    self.loc2gid[loc] = gid

        def _extract_all_refs(self):
            # extract all the references
            with open(self.ref_file) as rf:
                lines = filter(bool, rf.read().split('\n'))
                for line in lines:
                    gid = int(re.findall(r'\[(.*?)\]', line)[0])
                    name, loc, binding_str = re.findall(r'<(.*?)>', line)[:3]
                    if loc.startswith(self.projroot):
                        loc = loc[len(self.projroot):]
                        in_proj = True
                    else:
                        in_proj = False
                    bindings = {self.binding_table[int(b)] for b in binding_str.split('|')
                                if int(b) in self.binding_table}
                    if bindings:
                        ref = Reference(gid, name, loc, in_proj)
                        self.ref_table[gid] = ref
                        self.loc2gid[loc] = gid
                        for bind in bindings:
                            ref.add_binding(bind)

    projsroot = '/home/gui/Current/NamingProject/Benchmarks/'
    mdataroot = '/home/gui/Current/NamingProject/MData/'
    sdataroot = '/home/gui/Current/NamingProject/SData/'
    projdir = 'httptools/urllib3/'
    mdata_projpre = 'httptools-urllib3-mdata-'

    testor = TestDriver(projroot=join(projsroot, projdir),
                        binding_file=join(sdataroot, projdir, 'all-bindings.txt'),
                        ref_file=join(sdataroot, projdir, 'all-references.txt'))
    testor.test()
