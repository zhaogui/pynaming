#!/usr/bin/python
# _*_ coding:utf-8 _*_

import sys


class Tee(object):
    def __init__(self, name, mode):
        self.file = open(name, mode)
        self.stdout = sys.stdout
        sys.stdout = self

    def write(self, data):
        self.file.write(data)
        self.stdout.write(data)

    def __enter__(self):
        pass

    def __exit__(self, *_):
        sys.stdout = self.stdout
        self.file.close()