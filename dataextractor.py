#!/usr/bin/python
# _*_ coding:utf-8 _*_

__author__ = 'root'


from os.path import exists, isdir, isfile, join, splitext
from ast import *
from utils import encode_filename, print_progress
from dump_python import parse_file, get_source, improve_ast


try:
    import cPickle as pickle
except:
    import pickle


def _log(msg):
    print msg


def _get_source(filename):
    return get_source(filename)


def extract_file(filepath):
    print filepath
    asttree = parse_file(filepath)
    return NameExtractor(filepath).extract(asttree)


def extract_all(pkldir='pkls', tabfile='all-modules.txt',
                contains='', ignores='', outfile='extracted-data.txt'):
    allasts = _load_allasts(pkldir, tabfile, contains, ignores)
    infos = []
    allasts_len = len(allasts)
    _log(str(allasts_len) + ' files to be extracted...')
    for i, (pkl, asttree) in enumerate(allasts.iteritems()):
        print_progress(i + 1, allasts_len, suffix= "[Extract %s...]" % pkl, print_bar=False)
        #_log('(' + str(i+1) + ') Extract file: ' + pkl)
        if not hasattr(asttree, 'filename'):
            improve_ast(asttree, _get_source(pkl))
        infos.extend(NameExtractor(pkl).extract(asttree))
    _log('>>>Dumping to output file...')
    with open(outfile, 'w') as f:
        for info in infos:
            if info.types:
                print >>f, info
    return infos


def _load_allasts(pkldir, tabfile, contains, ignores):
    asts = {}
    if exists(pkldir) and isdir(pkldir) and \
            exists(join(pkldir, tabfile)):
        modules = []
        with open(join(pkldir, tabfile)) as f:
            for line in f.readlines():
                if ignores and (ignores in line):
                    continue
                if contains in line:
                    modules.append(line.split('\t=>\t')[0].strip())
        for pkl in modules:
            epklfile = encode_filename(pkl)
            epklfile = splitext(epklfile)[0] + '.pkl'
            if isfile(join(pkldir, epklfile)):
                with open(join(pkldir, epklfile)) as f:
                    asttree = pickle.load(f)
                    if asttree:
                        asts[pkl] = asttree
    return asts


class InfoPkg(object):
    def __init__(self, identifer, types, context, location):
        self.id = identifer
        self.types = types
        self.context = context
        self.location = location

    def __str__(self):
        return '{id} {types} {context} {location}'.format(id=self.id,
                                                          types='|'.join(self.types),
                                                          context=self.context,
                                                          location=self.location)


class Anchor(object):
    def __init__(self, construct):
        self.construct = construct

    def __str__(self):
        return 'Anchor:' + self.construct.__class__.__name__.lower()


class Link(object):
    def __init__(self, construct):
        self.construct = construct
        self.operator = 'name'

    def set_operator(self, op):
        if isinstance(op, str):
            self.operator = op
        else:
            self.operator = op.__class__.__name__.lower()

    def __str__(self):
        return 'Link:' + self.construct.__class__.__name__.lower() \
               + ':' + self.operator


class Parameter(object):
    def __init__(self, construct):
        self.construct = construct

    def __str__(self):
        return 'Param:' + self.construct.__class__.__name__.lower()


def get_shadow_type(name):
    return getattr(name, 'shadow', {}).get('type', [])


class NameExtractor(object):
    def __init__(self, filepath):
        self.filepath = filepath
        self.stmt_context = None

    def get_location(self, lineno, start, end):
        return '{filepath}#{lineno}:{start}-{end}'.format(filepath=self.filepath,
                                                          lineno=lineno,
                                                          start=start,
                                                          end=end)

    def extract(self, node):
        if not node: return []
        nodename = node.__class__.__name__.lower()
        oldstmtcontext = self.stmt_context
        if isinstance(node, stmt):
            self.stmt_context = node
        extract_func = getattr(self, 'extract_' + nodename, self.default_extract)
        infos = extract_func(node)
        self.stmt_context = oldstmtcontext
        return infos

    def default_extract(self, _):
        return []

    def extract_module(self, node):
        infos = []
        for stmt in node.body:
            infos.extend(self.extract(stmt))
        return infos

    def extract_functiondef(self, node):
        # TODO: extract the function name info
        infos = []
        arg_infos = self.extract(node.args)
        infos.extend(arg_infos)
        for stmt in node.body:
            infos.extend(self.extract(stmt))
        return infos

    def extract_classdef(self, node):
        # TODO: extract the class name info
        infos = []
        for stmt in node.body:
            infos.extend(self.extract(stmt))
        return infos

    def extract_return(self, node):
        return self.extract(node.value)

    def extract_delete(self, node):
        infos = []
        for tgt in node.targets:
            infos.extend(self.extract(tgt))
        return infos

    def extract_assign(self, node):
        infos = []
        for tgt in node.targets:
            infos.extend(self.extract(tgt))
        infos.extend(self.extract(node.value))
        return infos

    def extract_augassign(self, node):
        # simply ingore the operator here.
        infos = []
        infos.extend(self.extract(node.target))
        infos.extend(self.extract(node.value))
        return infos

    def extract_print(self, node):
        infos = []
        infos.extend(self.extract(node.dest))
        for val in node.values:
            infos.extend(self.extract(val))
        return infos

    def extract_for(self, node):
        infos = []
        infos.extend(self.extract(node.target))
        infos.extend(self.extract(node.iter))
        for stmt in node.body:
            infos.extend(self.extract(stmt))
        for stmt in node.orelse:
            infos.extend(self.extract(stmt))
        return infos

    def extract_while(self, node):
        infos= []
        infos.extend(self.extract(node.test))
        for stmt in node.body:
            infos.extend(self.extract(stmt))
        for stmt in node.orelse:
            infos.extend(self.extract(stmt))
        return infos

    def extract_if(self, node):
        infos = []
        infos.extend(self.extract(node.test))
        for stmt in node.body:
            infos.extend(self.extract(stmt))
        for stmt in node.orelse:
            infos.extend(self.extract(stmt))
        return infos

    def extract_with(self, node):
        infos = []
        infos.extend(self.extract(node.context_expr))
        infos.extend(self.extract(node.optional_vars))
        for stmt in node.body:
            infos.extend(self.extract(stmt))
        return infos

    def extract_raise(self, node):
        # simply ingore it for now.
        return []

    def extract_tryexcept(self, node):
        infos = []
        for stmt in node.body:
            infos.extend(self.extract(stmt))
        for stmt in node.orelse:
            infos.extend(self.extract(stmt))
        return infos

    def extract_tryfinally(self, node):
        infos = []
        for stmt in node.body:
            infos.extend(self.extract(stmt))
        for stmt in node.finalbody:
            infos.extend(self.extract(stmt))
        return infos

    def extract_assert(self, node):
        # simply ignore it for now
        return []

    def extract_import(self, node):
        # simply ingore it for now
        return []

    def extract_importfrom(self, node):
        # simply ingore it for now
        return []

    def extract_exec(self, node):
        # simply ingore it for now
        return []

    def extract_global(self, node):
        # simply ingore it for now
        return []

# ------------------------------- end of extract a stmt ---------------------------

    def extract_boolop(self, node):
        infos = []
        for val in node.values:
            if isinstance(val, Name):
                info = self.extract_name(val)[0]
                info.context.set_operator(node.op)
                infos.append(info)
            else:
                infos.extend(self.extract(val))
        return infos

    def extract_binop(self, node):
        infos = []
        if isinstance(node.left, Name):
            info = self.extract_name(node.left)[0]
            info.context.set_operator(node.op)
            infos.append(info)
        else:
            infos.extend(self.extract(node.left))

        if isinstance(node.right, Name):
            info = self.extract_name(node.right)[0]
            info.context.set_operator(node.right)
            infos.append(info)
        else:
            infos.extend(self.extract(node.right))
        return infos

    def extract_unaryop(self, node):
        infos = []
        if isinstance(node.operand, Name):
            info = self.extract_name(node.operand)[0]
            info.context.set_operator(node.op)
            infos.append(info)
        else:
            infos.extend(self.extract(node.operand))
        return infos

    def extract_lambda(self, node):
        infos = []
        infos.extend(self.extract(node.args))
        infos.extend(self.extract(node.body))
        return infos

    def extract_ifexp(self, node):
        infos = []
        infos.extend(self.extract(node.test))
        infos.extend(self.extract(node.body))
        infos.extend(self.extract(node.orelse))
        return infos

    def extract_dict(self, node):
        infos = []
        for key in node.keys:
            infos.extend(self.extract(key))
        for val in node.values:
            infos.extend(self.extract(val))
        return infos

    def extract_set(self, node):
        infos = []
        for elt in node.elts:
            infos.extend(self.extract(elt))
        return infos

    def extract_listcomp(self, node):
        infos = []
        infos.extend(self.extract(node.elt))
        for gen in node.generators:
            infos.extend(self.extract(gen))
        return infos

    def extract_setcomp(self, node):
        infos = []
        infos.extend(self.extract(node.elt))
        for gen in node.generators:
            infos.extend(self.extract(gen))
        return infos

    def extract_dictcomp(self, node):
        infos = []
        infos.extend(self.extract(node.key))
        infos.extend(self.extract(node.value))
        for gen in node.generators:
            infos.extend(self.extract(gen))
        return infos

    def extract_generatorexp(self, node):
        infos = []
        infos.extend(self.extract(node.elt))
        for gen in node.generators:
            infos.extend(self.extract(gen))
        return infos

    def extract_yield(self, node):
        infos = []
        infos.extend(self.extract(node.value))
        return infos

    def extract_compare(self, node):
        infos = []
        if isinstance(node, Name):
            info = self.extract_name(node.left)[0]
            info.context.set_operator(node.ops[0])
            infos.append(info)
        else:
            infos.extend(self.extract(node.left))
        for i, cmp in enumerate(node.comparators):
            if isinstance(cmp, Name):
                info = self.extract_name(cmp)[0]
                info.context.set_operator(node.ops[i])
                infos.append(info)
            else:
                infos.extend(self.extract(cmp))
        return infos

    def extract_call(self, node):
        infos = []
        infos.extend(self.extract(node.func))
        for arg in node.args:
            infos.extend(self.extract(arg))
        for kv in node.keywords:
            infos.extend(self.extract(kv.value))
        infos.extend(self.extract(node.starargs))
        infos.extend(self.extract(node.kwargs))
        return infos

    def extract_repr(self, node):
        return self.extract(node.value)

    def extract_attribute(self, node):
        infos = []
        if isinstance(node.value, Name):
            info = self.extract_name(node.value)[0]
            info.context.set_operator('dot')
            infos.append(info)
        else:
            infos.extend(self.extract(node.value))
        return infos

    def extract_subscript(self, node):
        infos = []
        if isinstance(node.value, Name):
            info = self.extract_name(node.value)[0]
            info.context.set_operator('subscript')
            infos.append(info)
        else:
            infos.extend(self.extract(node.value))
        # simply ignore the slice
        return infos

    def extract_name(self, node):
        infos = []
        if isinstance(node.ctx, (Load, Del, AugLoad)):
            info = InfoPkg(node.id, get_shadow_type(node), Link(self.stmt_context),
                           self.get_location(node.lineno, node.start, node.end))
            infos.append(info)
        elif isinstance(node.ctx, (Store, AugStore)):
            info = InfoPkg(node.id, get_shadow_type(node), Anchor(self.stmt_context),
                           self.get_location(node.lineno, node.start, node.end))
            infos.append(info)
        elif isinstance(node.ctx, Param):
            info = InfoPkg(node.id, get_shadow_type(node), Parameter(self.stmt_context),
                           self.get_location(node.lineno, node.start, node.end))
            infos.append(info)
        return infos

    def extract_list(self, node):
        infos = []
        for elt in node.elts:
            infos.extend(self.extract(elt))
        return infos

    def extract_tuple(self, node):
        infos = []
        for elt in node.elts:
            infos.extend(self.extract(elt))
        return infos

    def extract_arguments(self, node):
        infos = []
        for elt in node.args:
            infos.extend(self.extract(elt))
        return infos

    def extract_comprehension(self, node):
        infos = []
        infos.extend(self.extract(node.target))
        infos.extend(self.extract(node.iter))
        for ifexp in node.ifs:
            infos.extend(self.extract(ifexp))
        return infos




