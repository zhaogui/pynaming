#!/usr/bin/python
# _*_ coding:utf-8 _*_

import os
import sys
import inspect
from types import *
from ast import *
from os.path import abspath, sep, join


def get_filepaths(directory):
    """
    This function will generate the file names in a directory
    tree by walking the tree either top-down or bottom-up. For each
    directory in the tree rooted at directory top (including top itself),
    it yields a 3-tuple (dirpath, dirnames, filenames).
    """
    file_paths = []  # List which will store all of the full filepaths.

    # Walk the tree.
    for root, directories, files in os.walk(directory):
        for filename in files:
            # Join the two strings in order to form the full filepath.
            filepath = os.path.join(root, filename)
            file_paths.append(filepath)  # Add it to the list.

    return file_paths  # Self-explanatory.


def relative_path(rootdir, filepath):
    rel_path = filepath[len(rootdir):]
    if rel_path.startswith(sep):
        return rel_path[1:]
    else:
        return rel_path


def encode_filename(filename):
    return str(filename).replace(sep, '#')


def decode_filename(filename):
    return str(filename).replace('#', sep)


def istype(liveobj):
    return isinstance(liveobj, (TypeType, ClassType, ModuleType))


def getliveobject(frame, name):
    if name in frame.f_locals:
        return frame.f_locals[name]
    elif name in frame.f_globals:
        return frame.f_globals[name]
    else:
        builtins = frame.f_globals['__builtins__']
        if isinstance(builtins, dict) and (name in builtins):
            return builtins[name]
        elif isinstance(builtins, ModuleType) and hasattr(builtins, name):
            return getattr(frame.f_globals['__builtins__'], name)
    raise ValueError('cannot find the value of {name}'
                     ' in frame of {frame} in {fname}'.format(name=name,
                                                              frame=frame.f_code.co_name,
                                                              fname=frame.f_code.co_filename))


def getlivetype(frame, name):
    liveobj = getliveobject(frame, name)
    return getlivetype_from_obj(liveobj)


def getlivetype_from_obj(obj):
    try:
        return obj.__class__
    except AttributeError:
        return type(obj)


def getlivetypename(frame, name):
    try:
        livetype = getlivetype(frame, name)
        return gettypequalifiedname(livetype)
    except AttributeError:
        return None


def getlivetypename_from_obj(obj):
    try:
        livetype = getlivetype_from_obj(obj)
        return gettypequalifiedname(livetype)
    except AttributeError:
        return None

def gettypequalifiedname(livetype):
    if not hasattr(livetype, '__module__'):
        return livetype.__name__
    module = livetype.__module__
    if module is None or module == '__builtin__':
        return livetype.__name__
    return module + '.' + livetype.__name__

def getsourcefile(object_):
    if isinstance(object_, str):
        if str(object_[-4:]).lower() in ('.pyc', '.pyo'):
            return abspath(object_[:-4] + '.py')
        else:
            return abspath(object_)
    if inspect.isframe(object_):
        filename = inspect.getsourcefile(object_)
        return abspath(filename) if filename is not None else None
    else:
        return None


def getlineno2stmt(asttree):
    lineno2stmt = {}

    def alignlineno(node):
        for exprnode in walk(node):
            if hasattr(exprnode, 'lineno'):
                exprlineno = getattr(exprnode, 'lineno')
                if exprlineno > lineno:
                    lineno2stmt[exprlineno] = node

    for node in walk(asttree):
        if isinstance(node, stmt) and hasattr(node, 'lineno'):
            lineno = getattr(node, 'lineno')
            lineno2stmt[lineno] = node
            if not iscompoundstmt(node):
                alignlineno(node)
            elif isinstance(node, For):
                alignlineno(node.iter)
            elif isinstance(node, (While, If)):
                alignlineno(node.test)
            elif isinstance(node, With) and node.optional_vars:
                alignlineno(node.optional_vars)
    return lineno2stmt


def iscompoundstmt(node):
    return isinstance(node, (FunctionDef, ClassDef, For, While,
                             If, With, TryExcept, TryFinally))


def getastnode(line2stmt, lineno):
    return line2stmt.get(lineno)


def controlled_walk(node):
    """
    Controlled Recursively yield all descendant nodes in the tree starting at *node*
    (including *node* itself), in no specified order.  This is useful if you
    only want to modify nodes in place and don't care about the context.
    """
    from collections import deque
    todo = deque([node])
    while todo:
        node = todo.popleft()
        yield node
        signal = (yield)
        if signal:
            todo.extend(iter_child_nodes(node))


def dumpast(node, annotate_fields=True, include_attributes=False, shadow=True):
    """
    Return a formatted dump of the tree in *node*.  This is mainly useful for
    debugging purposes.  The returned string will show the names and the values
    for fields.  This makes the code impossible to evaluate, so if evaluation is
    wanted *annotate_fields* must be set to False.  Attributes such as line
    numbers and column offsets are not dumped by default.  If this is wanted,
    *include_attributes* can be set to True.
    """
    def _format(node, indent):
        sep = (', \n' if hasattr(node, 'body') else ', ')
        if isinstance(node, AST):
            fields = [(a, _format(b, indent+1)) for a, b in iter_fields(node)]
            rv = '%s(%s' % (node.__class__.__name__, sep.join(
                ('%s=%s' % field for field in fields)
                if annotate_fields else
                (b for a, b in fields)
            ))
            if include_attributes and node._attributes:
                rv += fields and ', ' or ' '
                rv += ', '.join('%s=%s' % (a, _format(getattr(node, a), indent+1))
                                for a in node._attributes)
            if shadow and hasattr(node, 'shadow'):
                rv += sep
                rv += 'shadow=%s' % getattr(node, 'shadow')
            return (rv + ')').replace('\n', '\n' + ' '*indent)
        elif isinstance(node, list):
            return '[%s]' % ',\n'.join(_format(x, indent+1) for x in node).replace('\n', '\n' + ' '*indent)
        return repr(node)
    if not isinstance(node, AST):
        raise TypeError('expected AST, got %r' % node.__class__.__name__)
    return _format(node, 1)


# Print iterations progress
def print_progress(iteration, total, prefix='Progress', suffix='Complete', decimals=2, print_bar=True, bar_length=30):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : number of decimals in percent complete (Int)
        barLength   - Optional  : character length of bar (Int)
    """
    filled_length = int(round(bar_length * iteration / float(total)))
    percents = round(100.00 * (iteration / float(total)), decimals)
    if print_bar:
        bar = '#' * filled_length + '-' * (bar_length - filled_length)
        sys.stdout.write('%s [%s] %s/%s(%s%s) %s\r' % (prefix, bar, iteration, total, percents, '%', suffix)),
    else:
        sys.stdout.write('%s: %s/%s(%s%s) %s\r' % (prefix, iteration, total, percents, '%', suffix))
    sys.stdout.flush()
    if iteration == total:
        print ""
        

libpaths = {'lib-root': 'lib/python2.7',
            'lib-distpkgs': 'lib/python2.7/dist-packages',
            'lib-sitepkgs': 'lib/python2.7/site-packages'}

preset_ignores = [
    join(libpaths['lib-root'], 're.py'),
    join(libpaths['lib-root'], 'hmac.py'),
    join(libpaths['lib-root'], 'threading.py'),
    join(libpaths['lib-root'], 'logging/'),
    join(libpaths['lib-root'], '_abcoll.py'),
    join(libpaths['lib-root'], '_weakrefset.py'),
    join(libpaths['lib-root'], 'sre_parse.py'),
    join(libpaths['lib-root'], 'sre_compile.py'),
    join(libpaths['lib-root'], 'collections.py'),
    join(libpaths['lib-root'], 'multiprocessing/'),
    join(libpaths['lib-root'], 'shlex.py'),
    join(libpaths['lib-root'], 'textwrap.py'),
    join(libpaths['lib-root'], 'encodings/'),
    join(libpaths['lib-sitepkgs'], 'six-1.8.0-py2.7.egg/six.py'),
    join(libpaths['lib-sitepkgs'], 'paramiko/'),
    join(libpaths['lib-sitepkgs'], 'yaml/'),
    join(libpaths['lib-sitepkgs'], 'chardet/'),
    join(libpaths['lib-sitepkgs'], 'Crypto/'),
    join(libpaths['lib-sitepkgs'], 'M2Crypto/'),
    join(libpaths['lib-sitepkgs'], 'zmq/'),
    join(libpaths['lib-sitepkgs'], 'pygments/lexers/'),
    join(libpaths['lib-root'], 'glob.py'),
    join(libpaths['lib-root'], 'fnmatch.py'),
    join(libpaths['lib-sitepkgs'], 'werkzeug/local.py'),
    join(libpaths['lib-sitepkgs'], 'werkzeug/datastructures.py'),
    join(libpaths['lib-sitepkgs'], 'jinja2'),
    join(libpaths['lib-sitepkgs'], 'py/'),
]
