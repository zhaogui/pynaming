#!/usr/bin/python
# _*_ coding:utf-8 _*_

__author__ = 'root'


import re
from os.path import splitext, sep
from utils import get_filepaths, relative_path


def dump_all_classes(sonar_data_rootdir, contains, output):
    # load static data
    sdata = set()
    allfiles = get_filepaths(sonar_data_rootdir)
    for filepath in allfiles:
        if filepath.endswith('.py.txt'):
            rpath = splitext(relative_path(sonar_data_rootdir, filepath))[0]
            if contains not in rpath:
                continue
            qpath = splitext(rpath)[0].replace(sep, '.')
            with open(filepath, 'rb') as f:
                lines = f.read().splitlines()
                for line in lines:
                    if line:
                        fields = re.findall('<<(.*?)>>', line)
                        id, cons, l,  t, kind = fields
                        if cons == 'CLASS' and kind == 'ANCHOR':
                            sdata.add('.'.join([qpath, id]))
                        elif t == 'type' and kind == 'LINK':
                            sdata.add(id)

    with open(output, 'w') as f:
        for d in sdata:
            print >>f, d