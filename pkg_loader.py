#!/usr/bin/python
# _*_ coding:utf-8 _*_

from os.path import join, dirname
import sys


pkgdir = join(dirname(__file__), 'pkgs')
sys.path.append(pkgdir)


from pgmpy.models import FactorGraph, MarkovModel
from pgmpy.factors import Factor
from pgmpy.inference import BeliefPropagation
