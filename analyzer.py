#!/usr/bin/python
# _*_ coding:utf-8 _*_

import re
import os
import sys
import bitypes
import numpy as np
from ast import *
from os import makedirs
from os.path import join, sep, exists, dirname
from datamining import TypeMinor
from dump_python import parse_file
from StringIO import StringIO
from itertools import combinations, groupby
from copy import deepcopy
from time import time
from multiprocessing.pool import Pool, TimeoutError
from multiprocessing.dummy import Pool as ThreadPool
from refcluster import cluster_refs, async_cluster_refs
from graphviz import Digraph
from math import sqrt
from pkg_loader import *
from utils import print_progress


def _log(msg, level='info'):
    if level == 'info':
        print >>sys.stdout, msg
    else:
        print >>sys.stderr, msg


class Node(object):
    def __init__(self, gid, name, loc, in_proj):
        self.gid = gid
        self.name = name
        self.loc = loc
        self.in_proj = in_proj
        self.attrs = set()
        self.types = set()  # type assumes
        self.extypes = set()  # exclude types

    def add_attr(self, attr):
        if attr not in bitypes.biattrs:
            self.attrs.add(attr)

    def add_type(self, tname):
        self.types.add(tname)

    def add_extype(self, tname):
        self.extypes.add(tname)

    # def __hash__(self):
    #     return self.gid

    def __eq__(self, other):
        if isinstance(other, Node):
            return other.gid == self.gid
        return False


class Binding(Node):
    def __init__(self, gid, name, loc, in_proj):
        super(Binding, self).__init__(gid, name, loc, in_proj)
        self.refs = set()
        self.srcs = set()
        self.dests = set()
        self.gid = gid

    def add_src(self, binding):
        self.srcs.add(binding)
        if self not in binding.dests:
            binding.add_dest(self)

    def add_dest(self, binding):
        self.dests.add(binding)
        if self not in binding.srcs:
            binding.add_src(self)

    def add_ref(self, ref):
        assert isinstance(ref, Reference), 'Only reference accepted, ' \
                                           '{} found.'.format(type(ref))
        self.refs.add(ref)
        if self not in ref.bindings:
            ref.add_binding(self)

    def __str__(self):
        return '[B{}]({})<{}>'.format(self.gid, self.name, self.loc)

    def __eq__(self, other):
        return isinstance(other, Binding) and self.gid == other.gid

    def __hash__(self):
        return hash('B' + str(self.gid))


class Reference(Node):
    def __init__(self, gid, name, loc, in_proj):
        super(Reference, self).__init__(gid, name, loc, in_proj)
        self.bindings = set()

    def add_binding(self, binding):
        assert isinstance(binding, Binding), 'Only reference accepted, ' \
                                             '{} found.'.format(type(binding))
        self.bindings.add(binding)
        if self not in binding.refs:
            binding.add_ref(self)

    def __str__(self):
        return '[R{}]({})<{}>'.format(self.gid, self.name, self.loc)

    def __eq__(self, other):
        return isinstance(other, Reference) and self.gid == other.gid

    def __hash__(self):
        return hash('R' + str(self.gid))


class Group(object):
    count = 0

    def __init__(self, *elts):
        self.elts = []
        self.attrs = set()
        self.types = set()
        self.extypes = set()
        self.add_elts(*elts)
        self.cid = Group.count
        Group.count += 1

    def add_elts(self, *elts):
        for elt in elts:
            assert isinstance(elt, (Binding, Reference)), \
                'Unknown type element of Group: ' + type(elt).__name__
            if elt not in self.elts:
                self.elts.append(elt)
                self.attrs.update(elt.attrs)
                self.types.update(elt.types)
                self.extypes.update(elt.extypes)

    @property
    def name(self):
        return self.elts[0].name if self.elts else ''

    def __str__(self):
        return '<' + '|'.join(str(elt) for elt in self.elts) + '>'

    def __eq__(self, other):
        return isinstance(other, Group) and self.cid == other.cid

    def __hash__(self):
        return self.cid


class ReferenceGroup(Group):
    def __init__(self, *elts):
        super(ReferenceGroup, self).__init__(*elts)
        self.bgroups = set()


class BindingGroup(Group):
    def __init__(self, *elts):
        super(BindingGroup, self).__init__(*elts)
        self.srcs = set()
        self.dests = set()
        self.rgroups = set()


class Cluster(object):
    count = 0

    def __init__(self, center, groups, node2groups, focus_groups, focus_nodes, direction):
        self.center = center
        self.groups = groups
        self.node2groups = node2groups
        self.focus_groups = focus_groups
        self.focus_nodes = focus_nodes
        self.focusgroup2probas = {}
        self.cid = Cluster.count
        self.direction = direction  # represent the searching direction
        Cluster.count += 1

    def add_focuses(self, *focuses):
        self.focus_groups.update(focuses)

    def query_proba(self, focus_node):
        tname2maxproba = {}
        for g in self.node2groups[focus_node]:
            for tname, proba in self.focusgroup2probas[g].iteritems():
                if tname not in tname2maxproba:
                    tname2maxproba[tname] = proba
                else:
                    if proba > tname2maxproba[tname]:
                        tname2maxproba[tname] = proba
        return tname2maxproba

    def query_all_focus_nodes(self):
        for node in self.focus_nodes:
            yield node, self.query_proba(node)

    @property
    def center_group(self):
        return list(self.node2groups[self.center])[0]

    @property
    def binding_groups(self):
        return filter(lambda x: isinstance(x, BindingGroup), self.groups)

    @staticmethod
    def _format_label_for_node(node):
        types = '|'.join(node.types)
        attrs = '|'.join(node.attrs)
        name = r'[{gid}]:{name}@{loc}'.format(gid=node.gid, name=node.name, types=types,
                                              attrs=attrs, loc=node.loc[:node.loc.index(':')])
        if types:
            name += ('[T: ' + types + ']')
        if attrs:
            name += ('[A: ' + attrs + ']')
        return name

    @staticmethod
    def _format_group_name(group=None):
        if isinstance(group, BindingGroup):
            return 'BC' + str(group.cid)
        elif isinstance(group, ReferenceGroup):
            return 'RC' + str(group.cid)

    def _format_group_label(self, group, benchmarks=()):
        label = StringIO()
        label.write('[' + self._format_group_name(group) + ']\\n')
        for node in group.elts:
            l = self._format_label_for_node(node)
            if node in self.focus_nodes:
                l += '***'
            elif node in benchmarks:
                l += l + '*'
            label.write(l + '\l')
        return r'' + label.getvalue()

    def _depict_group(self, group, benchmarks=()):
        name = self._format_group_name(group)
        label = self._format_group_label(group, benchmarks)
        shape = 'ellipse' if isinstance(group, BindingGroup) else 'box'
        node = group.elts[0]
        if node.loc.startswith('/'):
            return {'name': name, 'label': label, 'shape': shape, 'style': 'dotted'}
        else:
            return {'name': name, 'label': label, 'shape': shape, 'style': 'solid'}

    def draw(self, output_dir, benchmarks=()):
        name = r'type-constriants-graph-' + str(self.cid)
        filename = join(output_dir, 'tcons-' + str(self.cid) + '.gv')
        g = Digraph(name=name, filename=filename)
        g.body.extend(['rankdir=RL', 'size="6,6"'])
        for group in self.groups:
            g.node(**self._depict_group(group, benchmarks))

        for dest in self.binding_groups:
            dest_node = self._format_group_name(dest)
            for src in dest.srcs:
                g.edge(self._format_group_name(src), dest_node)
            for src in dest.rgroups:
                g.edge(self._format_group_name(src), dest_node)
        g.render()

    # @staticmethod
    # def _format_name_for_node(group=None, node=None):
    #     assert group or node, 'group and node should not be both none'
    #     name = ''
    #     if isinstance(group, BindingGroup):
    #         name = 'cluster_' + str(group.cid)
    #     elif isinstance(group, ReferenceGroup):
    #         name = 'cluster_' + str(group.cid)
    #
    #     if isinstance(node, Binding):
    #         name += ('-B' + str(node.gid))
    #     elif isinstance(node, Reference):
    #         name += ('-R' + str(node.gid))
    #     return name

    # def _depict_node(self, group, node, benchmarks=()):
    #     name = self._format_name_for_node(group, node)
    #     label = self._format_label_for_node(node)
    #     shape = 'ellipse' if isinstance(node, Binding) else 'box'
    #     if node.loc.startswith('/'):
    #         return {'name': name, 'label': label, 'shape': shape, 'style': 'dotted'}
    #     elif node in self.focus_nodes:
    #         return {'name': name, 'label': label, 'shape': shape, 'style': 'filled', 'fillcolor': 'skyblue'}
    #     elif node in benchmarks:
    #         return {'name': name, 'label': label, 'shape': shape, 'style': 'filled'}
    #     else:
    #         return {'name': name, 'label': label, 'shape': shape, 'style': 'solid'}

    # def _depict_group(self, group, benchmarks=()):
    #     name = self._format_name_for_node(group)
    #     c0 = Digraph(name)
    #     # c0.body.extend(['rankdir=TB', 'size="6,6"'])
    #     for node in group.elts:
    #         c0.node(**self._depict_node(group, node, benchmarks))
    #     if len(group.elts) >= 2:
    #         c0.edge(self._format_name_for_node(group, group.elts[0]),
    #                 self._format_name_for_node(group, group.elts[1]),
    #                 style='invis')
    #     c0.body.append('label = "' + name + '"')
    #     c0.body.append('color=blue')
    #     return c0

    # def draw(self, benchmarks=()):
    #     name = r'type-constriants-graph-' + str(self.cid)
    #     filename = 'dot/tcons-' + str(self.cid) + '.gv'
    #     g = Digraph(name=name, filename=filename)
    #     g.body.extend(['compound=true'])
    #     g.body.extend(['rankdir=TB', 'size="6,6"'])
    #     for group in self.groups:
    #         sg = self._depict_group(group, benchmarks)
    #         g.subgraph(sg)
    #
    #     for dest in self.binding_groups:
    #         dest_node = self._format_name_for_node(dest, dest.elts[0])
    #         for src in dest.srcs:
    #             g.edge(self._format_name_for_node(src, src.elts[0]), dest_node,
    #                    ltail=self._format_name_for_node(src),
    #                    lhead=self._format_name_for_node(dest))
    #         for src in dest.rgroups:
    #             g.edge(self._format_name_for_node(src, src.elts[0]), dest_node,
    #                    ltail=self._format_name_for_node(src),
    #                    lhead=self._format_name_for_node(dest))
    #     g.render()

    def __hash__(self):
        return self.cid

    def __str__(self):
        return str(self.cid)


class Type(object):
    def __init__(self, name, attrs):
        self.name = name
        self.attrs = attrs



def belief_probas_computer(args):
    t, g_focuses, graphical_model = args
    if len(graphical_model.node.keys()) < 1000:
        return _belief_probas_computer(t, g_focuses, graphical_model)
    # else:
        # p = ThreadPool(1)
        # res = p.apply_async(_belief_probas_computer, args=(t, g_focuses, graphical_model))
        # try:
            # out = res.get(timeout=10)  # Wait timeout seconds for func to complete.
            # return out
        # except TimeoutError:
            # _log('abort computing for timeout')
            # p.terminate()
            # return t, {k: np.array([1.0, 0.0]) for k in g_focuses}


def _belief_probas_computer(t, g_focuses, graphical_model):
    try:
        inference = BeliefPropagation(graphical_model)
        return t, {k: v.values for k, v in inference.query(g_focuses).iteritems()}
    except (TypeError, StopIteration, MemoryError) as e:
        # import traceback
        # print traceback.format_exc()
        # _log('Belief Computation Error: ' + type(e).__name__)
        return t, {k: np.array([1.0, 0.0]) for k in g_focuses}


class ProbTypeAnalyzer(object):
    
    ATTR_LEVEL, SUBTYPE_LEVEL, DATAFLOW_LEVEL, NAMING_LEVEL = 1, 2, 3, 4
    
    def __init__(self, projname, projroot, dynamic_data_dir, static_data_dir, merging_output_dir,
                 dynamic_data_contains='', dynamic_data_ignores='',
                 merging_ignored_dirs=(), merging_ignored_types=(),
                 merging_changed_types=(), merging_changed_data=(), async_refcluster=False, max_cluster_size=15,
                 merging_dynamic_loading_dis=2, proc_num=2, high_proba=0.95, naming_belief_proba=0.7, level=5):

        # project info, including source location and project name
        self.projname = projname
        self.projroot = projroot

        # collected data directories. For DataMerger
        self.dynamic_data_dir = dynamic_data_dir
        self.static_data_dir = static_data_dir
        self.dynamic_data_contains = dynamic_data_contains
        self.dynamic_data_ignores = dynamic_data_ignores
        self.merging_output_dir = merging_output_dir
        
        # data filters
        self.ignored_dirs = merging_ignored_dirs
        self.ignored_types = merging_ignored_types
        self.changed_types = merging_changed_types
        self.changed_data = merging_changed_data
        
        # dynamic loading distance. For DataMerger
        self.dynamic_loading_dis = merging_dynamic_loading_dis

        # process number
        self.proc_num = proc_num

        # NLP Miner
        self.minor = None
        
        # reference clustering
        self.async_refcluster = async_refcluster
        
        # maximum handled cluster size
        self.max_cluster_size = max_cluster_size
        
        # files produced by DataMerger
        self.training_file = join(self.merging_output_dir, 'same.txt')
        self.testing_file = join(self.merging_output_dir, 'diff.txt')
        self.sdata_file = join(self.merging_output_dir, 'sdata.txt')
        self.statically_collected_types_file = join(self.merging_output_dir, 'stypes.txt')
        self.dynamically_collected_types_file = join(self.merging_output_dir, 'dtypes.txt')
        self.dynamically_collected_type_hierarchies_file = join(self.merging_output_dir, 'dsubtypes.txt')

        # files of all bindings, references and type constraints collected by static tool
        self.all_bindings_file = join(self.static_data_dir, 'all-bindings.txt')
        self.all_references_file = join(self.static_data_dir, 'all-references.txt')
        self.all_type_constraints_file = join(self.static_data_dir, 'all-constraints.txt')
        self.all_type2attrs_file = join(self.static_data_dir, 'all-typeattrs.txt')

        # data members
        self.all_data_with_stypes = {}  # loc+id -> stypes
        self.binding_table = {}  # gid -> binding
        self.ref_table = {}  # gid -> reference
        self.nlp_prob_table = {}  # ref|binding -> proba of each type

        # dynamic(oracle) and static collected data
        self.node2dtypes = {}
        self.node2stypes = {}
        self.benchmarks = {}

        self.loc2gid = {}  # location -> gid
        self.type2attrs = {}  # type -> attrs
        self.type2subtypes = {}  # type -> subtypes

        self.node2predprobas = {}  # ref|binding -> dict of proba of each type

        self.clusters = None
        self.node2clusters = {}
        self.binding2refgroups = {}

        self.log_dir = join('log', self.projname)
        if not exists(self.log_dir):
            makedirs(self.log_dir)
        self.cluster_dir = join('dot', self.projname)
        if not exists(self.cluster_dir):
            makedirs(self.cluster_dir)

        self.attrs2unique = {}
        
        self.high_proba = high_proba
        self.naming_belief_proba = naming_belief_proba

        # for matrix
        self.cluster2time = {}
        self.node2time = {}
        self.thre2matrix = {}
        self.gap2matrix = {}
        self.time_matrix = ()
        self.two_naive_accuracies = []

        # for debugging
        self.cluster2pgmodels = {}
        
        # level of considering constraints
        # 1: only attribute
        # 2: +subtyping
        # 3: +dataflow
        # 4: +naming 
        self.level=level

    # phase 1 merge data both from static and dynamic data
    def merge_data(self):
        from datamerger_new import DataMerger
        dmerger = DataMerger(projroot=self.projroot, projname=self.projname,
                             dynamic_data_dir=self.dynamic_data_dir,
                             static_data_dir=self.static_data_dir,
                             output_dir=self.merging_output_dir,
                             dynamic_data_contains=self.dynamic_data_contains,
                             dynamic_data_ignores=self.dynamic_data_ignores,
                             merging_ignored_dirs=self.ignored_dirs,
                             merging_ignored_types=self.ignored_types,
                             merging_changed_types=self.changed_types,
                             merging_changed_data=self.changed_data,
                             dynamic_loading_dis=self.dynamic_loading_dis)
        dmerger.merge_data()

    def mine_data(self):
        _log('>>>Training the NLP model...')
        training_file = join(self.merging_output_dir, 'same.txt')
        testing_file = join(self.merging_output_dir, 'diff.txt')
        stypes_file = join(self.merging_output_dir, 'stypes.txt')
        self.minor = TypeMinor(training_file, testing_file, stypes_file)
        self.minor.train()
        _log('>>>Evaluating all test data...')
        self.minor.evaluate_all_tests()
        # _log('>>>Dumping all mining results...')
        # with open(join(self.log_dir, 'mining-results.txt'), 'w') as f:
        #     for d in self.minor.results:
        #         id_str = "(%s)<%s>" % (d['id'], d['location'])
        #         dtypes = "dtypes:" + str(d['dtypes'])
        #         ptypes = "ptypes:[" + ','.join((t + ':' + str(d['pred_proba'][t])) \
        #                                        for t in d['ptypes'] if t in d['pred_proba']) + "]"
        #         print >>f, id_str, dtypes, ptypes
        # _log('>>>Making summaries...')
        # from pynaming.logger import Tee
        # with Tee(join('log', self.projname, 'mining-summaries.txt'), 'w'):
        #     self.minor.make_summary()
        _log('>>>Making summaries...')
        self.minor.make_summary()
        
    def extract_raw_data(self):
        _log('>>>Extracting all statically inferred types')
        self._extract_all_statically_inferred_types()
        _log('>>>Extracting all bindings...')
        self._extract_all_bindings()
        _log('>>>Extracting all references...')
        self._extract_all_refs()
        _log('>>>Extracting all constraints...')
        self._extract_all_constraints()
        _log('>>>Extracting builtin type hierarchies...')
        self.type2subtypes.update(dict(bitypes.builtin_type2subtypes))
        _log('>>>Extracting type to subtypes from dynamic loading...')
        self._extract_dynamically_loading_collected_subtypes()
        _log('>>>Extracting the preconditional probabilities and benchmarks from minor')
        self._extract_nlp_probas_and_benchmarks()
        _log('>>>Extracting the attribute set of each type')
        self._extract_type2attrs_from_statically_collected_data()
        self._extract_type2attrs_and_typehierarchies_from_scanning_sources()
        self._extract_type2attrs_from_dynamically_loading()
        # self._dump_type2attrs()
        _log('>>>Refilling the attributes and type-assumptions used for references')
        self._refill_attrs_and_types()
        _log('---------summaries before analyzing----------')
        self._make_summary()

    def _extract_all_statically_inferred_types(self):
        # extract all the static types
        with open(self.sdata_file) as sf:
            lines = filter(bool, sf.read().split('\n'))
            basepath_len = len(self.projroot) if self.projroot.endswith('/') \
                else len(self.projroot) + 1
            for line in lines:
                name, stypes, cnt, loc = line.split()
                if self.projroot in loc:
                    loc = loc[basepath_len:]
                else:
                    continue
                self.all_data_with_stypes[loc + '@' + name] = set(stypes.split('|'))

    def _extract_all_bindings(self):
        # extract all the bindings
        with open(self.all_bindings_file) as bf:
            lines = filter(bool, bf.read().split('\n'))
            for line in lines:
                gid = int(re.findall(r'\[(.*?)\]', line)[0])
                name, loc = re.findall(r'<(.*?)>', line)[:2]
                if loc.startswith(self.projroot):
                    if self.projroot.endswith(sep):
                        start = len(self.projroot)
                    else:
                        start = len(self.projroot) + 1
                    loc = loc[start:]
                    in_proj = True
                else:
                    in_proj = False
                self.binding_table[gid] = Binding(gid, name, loc, in_proj)
                self.loc2gid[loc] = gid

    def _extract_all_refs(self):
        # extract all the references
        with open(self.all_references_file) as rf:
            lines = filter(bool, rf.read().split('\n'))
            for line in lines:
                gid = int(re.findall(r'\[(.*?)\]', line)[0])
                name, loc, binding_str = re.findall(r'<(.*?)>', line)[:3]
                if loc.startswith(self.projroot):
                    if self.projroot.endswith(sep):
                        start = len(self.projroot)
                    else:
                        start = len(self.projroot) + 1
                    loc = loc[start:]
                    in_proj = True
                else:
                    in_proj = False
                bindings = {self.binding_table[int(b)] for b in binding_str.split('|')
                            if int(b) in self.binding_table}
                if bindings:
                    ref = Reference(gid, name, loc, in_proj)
                    self.ref_table[gid] = ref
                    self.loc2gid[loc] = gid
                    for bind in bindings:
                        ref.add_binding(bind)

    def _extract_all_constraints(self):
        # extract all the constraints
        with open(self.all_type_constraints_file) as cf:
            lines = filter(bool, cf.read().split('\n'))
            for line in lines:
                all_gids = map(int, re.findall(r'\[(.*?)\]', line))
                source, targets = all_gids[0], all_gids[1:]
                if source in self.binding_table:
                    source = self.binding_table[source]
                    for tgt in targets:
                        if tgt in self.binding_table:
                            source.add_dest(self.binding_table[tgt])

    def _extract_nlp_probas_and_benchmarks(self):
        for result in self.minor.results:
            var, cnt, loc = result['id'], result['context'], result['location']
            proba, dtypes, stypes = result['pred_proba'], result['dtypes'], result['stypes']
            if loc not in self.loc2gid:
                continue
            gid = self.loc2gid[loc]
            if ('Link' in cnt) and (gid in self.ref_table):
                self.nlp_prob_table[self.ref_table[gid]] = proba
                self.node2dtypes[self.ref_table[gid]] = dtypes
                if stypes:
                    self.node2stypes[self.ref_table[gid]] = stypes
            elif gid in self.binding_table:
                self.nlp_prob_table[self.binding_table[gid]] = proba
                self.node2dtypes[self.binding_table[gid]] = dtypes
                if stypes:
                    self.node2stypes[self.binding_table[gid]] = stypes
        self.benchmarks = set(self.node2dtypes.keys())

    def _extract_dynamically_loading_collected_subtypes(self):
        with open(self.dynamically_collected_type_hierarchies_file) as f:
            lines = f.read().splitlines()
            for line in lines:
                t, subs = line.split()
                if t in self.type2subtypes:
                    self.type2subtypes[t].update(set(subs.split('|')))
                else:
                    self.type2subtypes[t] = set(subs.split('|'))

    def _extract_type2attrs_from_statically_collected_data(self):
        # types = set(self.minor.type2id.iterkeys())
        types = set()
        with open(self.statically_collected_types_file) as f:
            lines = f.read().splitlines()
            for t in lines:
                if t.strip():
                    types.add(t.strip())
        # add some common types into the type domain
        types.update(bitypes.types.keys())

        self.type2attrs = {t: set() for t in types}
        with open(self.all_type2attrs_file) as af:
            lines = filter(bool, af.read().split('\n'))
            for line in lines:
                tname, attrs = re.findall('<(.*?)>', line)
                if tname in types:
                    self.type2attrs[tname].update(set(attrs.split('|')))

    def _extract_type2attrs_and_typehierarchies_from_scanning_sources(self):
        class2attrs, class2subtypes = self._collect_classdef_type2attrs_and_typehierarchies()
        for tname, attrs in self.type2attrs.iteritems():
            if tname in bitypes.types:
                attrs.update(bitypes.types[tname])
            if tname in class2attrs:
                attrs.update(class2attrs[tname])

        for cname, subclasses in class2subtypes.iteritems():
            if cname in self.type2subtypes:
                self.type2subtypes[cname].update(subclasses)
            else:
                self.type2subtypes[cname] = subclasses

    def _extract_type2attrs_from_dynamically_loading(self):
        # add dynamically collected types
        with open(self.dynamically_collected_types_file) as df:
            lines = filter(bool, df.read().split('\n'))
            for line in lines:
                tname, attrs = line.split()
                if tname in self.type2attrs:
                    self.type2attrs[tname].update(set(attrs.split('|')))
                else:
                    self.type2attrs[tname] = set(attrs.split('|'))

    def _collect_classdef_type2attrs_and_typehierarchies(self):
        class2asts = {}
        class2attrs = {}
        class2subclasses = {}

        # collect all class defs
        for root, _, files in os.walk(self.projroot):
            for f in files:
                filepath = join(root, f)
                if filepath.endswith('.py'):
                    try:  # Fix: the file parser(dump_python) has some bugs
                        asttree = parse_file(filepath)
                    except:
                        continue
                    for node in walk(asttree):
                        if isinstance(node, ClassDef):
                            node.filename = filepath
                            if node.name not in class2asts:
                                class2asts[node.name] = {node}
                            else:
                                class2asts[node.name].add(node)

        def find_root(base):
            if isinstance(base, Attribute):
                return find_root(base.value)
            elif isinstance(base, Name):
                return base.id
            else:
                return ''

        # recusively collect the attrs and subclasses of classes
        def _collect_attrs(clsdef, pending=()):
            attrs = set()
            # collect attrs from bases
            for base in clsdef.bases:
                bname = ''
                if isinstance(base, Name):
                    bname = base.id
                elif isinstance(base, Attribute):
                    root = ''
                    if isinstance(base.value, Attribute):
                        root = find_root(base.value)
                    if root == self.projname:
                        bname = base.attr
                if bname:
                    if bname in class2attrs:
                        attrs.update(class2attrs[bname])
                    elif bname in class2asts:
                        if bname not in pending:
                            pending = set(pending).union({bname})
                            base_attrs = set()
                            for c in class2asts[bname]:
                                base_attrs.update(_collect_attrs(c, pending))
                            attrs.update(base_attrs)
            # collect attrs from the methods
            for node in walk(clsdef):
                if isinstance(node, Attribute) and \
                    isinstance(node.value, Name) and \
                        node.value.id in ('self', 'cls'):
                    attrs.add(node.attr)
            class2attrs[clsdef.name] = attrs
            return attrs

        def _collect_subtypes(clsdef):
            for base in clsdef.bases:
                if isinstance(base, Name):
                    bname = base.id
                elif isinstance(base, Attribute):
                    bname = base.attr
                else:
                    bname = ''
                if bname and bname != 'object':
                    if bname in class2subclasses:
                        class2subclasses[bname].add(clsdef.name)
                    else:
                        class2subclasses[bname] = {clsdef.name}

        # collect the attr-set of each class
        for cname, cdefs in class2asts.iteritems():
            if cname in class2attrs:
                continue
            for cdef in cdefs:
                _collect_attrs(cdef)
                _collect_subtypes(cdef)
        # for cls, attrs in class2attrs.iteritems():
        #     print cls, attrs
        return class2attrs, class2subclasses

    def _dump_type2attrs(self):
        _log('>>>Dumping all the types and attrs')
        with open(join(self.log_dir, 'type2attrs.txt'), 'w') as tf:
            for tname, attrs in self.type2attrs.iteritems():
                print >>tf, '[' + tname + ']', '[' + '|'.join(attrs) + ']'

    def _refill_attrs_and_types(self):
        #TODO: This method should be refactored in the future.
        for root, _, files in os.walk(self.projroot):
            for f in files:
                filepath = join(root, f)
                relative_filepath = self._get_relative_path(filepath)
                if filepath.endswith('.py'):
                    try:  # the parse_file may have some bugs
                        asttree = parse_file(filepath)
                    except:
                        continue
                    for node in walk(asttree):
                        if isinstance(node, Attribute) and \
                                isinstance(node.ctx, Load):
                            self._add_attr(filepath, node.value, node.attr)
                        elif isinstance(node, BinOp):
                            # self._add_attr(filepath, node.left, self.map_astop_mthd(node.op))
                            # self._add_attr(filepath, node.right, self.map_astop_mthd(node.op))
                            if isinstance(node.left, (Name, Attribute)):
                                if isinstance(node.right, Str):
                                    self._add_type(filepath, node.left, 'str')
                                elif isinstance(node.right, Num):
                                    self._add_type(filepath, node.left, node.right.num_type)
                                elif isinstance(node.right, Name):
                                    loc = '{filepath}#{lno}:{start}-{end}@{id}'.format(filepath=relative_filepath,
                                                                                       lno=node.right.lineno,
                                                                                       start=node.start, end=node.end,
                                                                                       id=node.right.id)
                                    if loc in self.all_data_with_stypes:
                                        self._add_type(filepath, node.right, self.all_data_with_stypes[loc])
                            if isinstance(node.right, (Name, Attribute)):
                                if isinstance(node.left, Str):
                                    if isinstance(node.op, Mod):
                                        if '%s' in node.left.s or '%c' in node.left.s:
                                            self._add_type(filepath, node.right, 'str')
                                        elif ('%d' in node.left.s) or ('%i' in node.left.s):
                                            self._add_type(filepath, node.right, 'int')
                                        elif '%f' in node.left.s:
                                            self._add_type(filepath, node.right, 'float')
                                    elif isinstance(node.op, Mult):
                                        self._add_type(filepath, node.right, 'int')
                                    else:
                                        self._add_type(filepath, node.right, 'str')
                                elif isinstance(node.left, Num):
                                    self._add_type(filepath, node.right, node.left.num_type)
                                elif isinstance(node.right, Name):
                                    #TODO: we need more precision in the future
                                    loc = '{filepath}#{lno}:{start}-{end}@{id}'.format(filepath=relative_filepath,
                                                                                       lno=node.right.lineno,
                                                                                       start=node.start, end=node.end,
                                                                                       id=node.right.id)
                                    if loc in self.all_data_with_stypes:
                                        self._add_type(filepath, node.right, self.all_data_with_stypes[loc])
                        elif isinstance(node, Subscript):
                            if isinstance(node.slice, Index):
                                if isinstance(node.slice.value, Str):
                                    self._add_type(filepath, node.value, 'dict')
                                    self._add_exclude(filepath, node.value, {'list', 'tuple', 'str'})
                                if isinstance(node.slice.value, Num):
                                    self._add_type(filepath, node.value, {'list'})
                                # TODO: we should also add processing for the case of Name
                            self._add_attr(filepath, node.value, '__getitem__')
                        elif isinstance(node, UnaryOp):
                            attr = self.map_astop_mthd(node.op)
                            if attr:
                                self._add_attr(filepath, node.operand, attr)
                            elif isinstance(node.op, Not):
                                self._add_type(filepath, node.operand, {'None', 'bool', 'str', 'int'})
                        elif isinstance(node, (For, comprehension)):
                            self._add_attr(filepath, node.iter, '__iter__')
                        elif isinstance(node, If):
                            self._add_type(filepath, node.test, {'None', 'bool', 'str', 'int'})
                            # self._add_type(filepath, node.iter, {'list', 'set', 'tuple', 'dict'})
                        elif isinstance(node, With):
                            self._add_attr(filepath, node.context_expr, '__enter__')
                            self._add_attr(filepath, node.context_expr, '__exit__')
                        elif isinstance(node, Call):
                            self._add_attr(filepath, node.func, '__code__')
                            if self.level >= self.SUBTYPE_LEVEL:
                                if isinstance(node.func, Name) and node.func.id == 'isinstance':
                                    if len(node.args) == 2:
                                        self._add_subtype(filepath, node.args[0], node.args[1])
                                if isinstance(node.func, Name) and node.func.id == 'hasattr':
                                    if len(node.args) == 2 and isinstance(node.args[1], Str):
                                        self._add_attr(filepath, node.args[0], node.args[1].s)
                            func = ''
                            if isinstance(node.func, Name):
                                func = node.func.id
                            elif isinstance(node.func, Attribute):
                                func = node.func.attr
                            if func and (func in bitypes.param_types):
                                params = bitypes.param_types[func]
                                for i, arg in enumerate(node.args):
                                    if i in params:
                                        if func in ('min', 'max'):
                                            if len(params) > 1:
                                                self._add_type(filepath, arg, params[i].get('types', None))
                                            else:
                                                self._add_attr(filepath, arg, params[i].get('attrs', None))
                                        else:
                                            self._add_attr(filepath, arg, params[i].get('attrs', None))
                                            self._add_type(filepath, arg, params[i].get('types', None))

                        elif isinstance(node, Compare):
                            if isinstance(node.ops[0], (Is, IsNot, Eq, NotEq, Gt, GtE, Lt, LtE)):
                                # we only care two operands.
                                # TODO: we may perfect this in the future.
                                if isinstance(node.comparators[0], Name):
                                    var = node.comparators[0].id
                                    if var == 'None':
                                        self._add_type(filepath, node.left, node.comparators[0])
                                    elif var in ('True', 'False'):
                                        self._add_type(filepath, node.left, 'bool')
                                    else:
                                        loc = '{filepath}#{lno}:{start}-{end}' \
                                              '@{id}'.format(filepath=relative_filepath,
                                                             lno=node.comparators[0].lineno,
                                                             start=node.comparators[0].start,
                                                             end=node.comparators[0].end,
                                                             id=var)
                                        if loc in self.all_data_with_stypes:
                                            self._add_type(filepath, node.left, self.all_data_with_stypes[loc])
                                elif isinstance(node.comparators[0], Num):
                                    self._add_type(filepath, node.left, node.comparators[0].num_type)
                                elif isinstance(node.comparators[0], Str):
                                    self._add_type(filepath, node.left, 'str')
                            elif isinstance(node.ops[0], (In, NotIn)):
                                self._add_attr(filepath, node.comparators[0], {'__contains__'})
                                if isinstance(node.left, Str):
                                    self._add_type(filepath, node.comparators[0], 'str')
               
                        # elif isinstance(node, BoolOp):
                        #     for val in node.values:
                        #         self._add_type(filepath, val, {'None', 'bool', 'str', 'int'})
                        elif isinstance(node, Assign):
                            if isinstance(node.targets[0], Tuple):
                                self._add_type(filepath, node.value, {'list', 'tuple'})
                            if isinstance(node.value, Call):
                                if isinstance(node.value.func, Name):
                                    func = node.value.func.id
                                elif isinstance(node.value.func, Attribute):
                                    func = node.value.func.attr
                                if func and (func in bitypes.param_types):
                                    params = bitypes.param_types[func]
                                    if -1 in params:
                                        self._add_type(filepath, node.targets[0],
                                                       params.get(-1).get('types', None))
                                if node.value.starargs:
                                    self._add_type(filepath, node.value.starargs, {'list', 'tuple'})
                                if node.value.kwargs:
                                    self._add_type(filepath, node.value.kwargs, {'dict'})
                            elif isinstance(node.value, Name):
                                if node.value.id == 'None':
                                    self._add_type(filepath, node.targets[0], {'None'})
                                elif node.value.id in ('False', 'True'):
                                    self._add_type(filepath, node.targets[0], {'bool'})
                            elif isinstance(node.value, Str):
                                self._add_type(filepath, node.targets[0], 'str')
                            elif isinstance(node.value, Num):
                                self._add_type(filepath, node.targets[0], node.value.num_type)

    def _get_relative_path(self, fullpath):
        basepath_len = len(self.projroot) if self.projroot.endswith(sep) \
            else len(self.projroot) + 1
        if self.projroot in fullpath:
            path = fullpath[basepath_len:]
        else:
            path = fullpath
        return path

    def _find_location(self, filepath, node):
        if isinstance(node, Name):
            start = node.start
        else:  # Atribute:
            start = node.end - len(node.attr) + 1

        loc = '{file}#{lno}:{start}-{end}'.format(file=self._get_relative_path(filepath),
                                                  lno=node.lineno, start=start, end=node.end)
        return loc

    def _find_node(self, filepath, node):
        gnode = None
        if isinstance(node, (Name, Attribute)):
            loc = self._find_location(filepath, node)
            if loc in self.loc2gid:
                gid = self.loc2gid[loc]
                if gid in self.ref_table:
                    gnode = self.ref_table[gid]
                elif gid in self.binding_table:
                    gnode = self.binding_table[gid]
        return gnode

    def _add_attr(self, filepath, node, attr):
        if not attr:
            return False
        gnode = self._find_node(filepath, node)
        if gnode:
            if hasattr(attr, '__iter__'):
                for a in attr:
                    gnode.add_attr(a)
            else:
                gnode.add_attr(attr)
            return True
        return False

    def _add_exclude(self, filepath, node, extype):
        if not extype:
            return False
        gnode = self._find_node(filepath, node)
        if gnode:
            if hasattr(extype, '__iter__'):
                for e in extype:
                    gnode.add_extype(e)
            else:
                gnode.add_extype(extype)
            return True
        return False

    def _normalize_types(self, types):
        new_types = set()
        for t in types:
            if t in self.ignored_types:
                continue
            elif t in self.changed_types:
                t = self.changed_types[t]
            new_types.add(t)
        return new_types

    def _add_type(self, filepath, node, tnode):
        if not tnode:
            return False
        gnode = self._find_node(filepath, node)
        if gnode:
            if isinstance(tnode, Attribute):
                tnames = {tnode.attr}
            elif isinstance(tnode, Name):
                tnames = {tnode.id}
            elif isinstance(tnode, str):
                tnames = {tnode}
            elif hasattr(tnode, '__iter__'):
                tnames = set(tnode)
            else:
                tnames = {}
            for tname in self._normalize_types(tnames):
                gnode.add_type(tname)
            return True
        return False

    def _collect_all_subtypes(self, base, pending=()):
        if base not in self.type2subtypes:
            return set()
        subtypes = set()
        for s in self.type2subtypes[base]:
            subtypes.add(s)
            if s not in pending:
                pending = set(pending).union({s})
                subtypes.update(self._collect_all_subtypes(s, pending))
        return subtypes

    def _add_subtype(self, filepath, node, tnode):
        if not tnode:
            return False
        gnode = self._find_node(filepath, node)
        if gnode:
            if isinstance(tnode, Attribute):
                tname = tnode.attr
            elif isinstance(tnode, Name):
                tname = tnode.id
            else:
                tname = ''
            if tname:
                subttypess = self._collect_all_subtypes(tname)
                alltypes = subttypess.union({tname})
                for t in self._normalize_types(alltypes):
                    gnode.add_type(t)
        if isinstance(tnode, Tuple):
            for elt in tnode.elts:
                self._add_subtype(filepath, node, elt)

    def _make_summary(self):
        # all binding summaries
        b_len = len(self.binding_table)
        pb_len = 0
        max_ref = 0
        for b in self.binding_table.itervalues():
            if not b.loc.startswith('/'):
                pb_len += 1
                if len(b.refs) > max_ref:
                    max_ref = len(b.refs)
        _log('Total binding: {}, with {}({:.0%}) items and ({}) max number of '
             'refs in the project'.format(b_len, pb_len, 1.0 * pb_len / b_len, max_ref))
        # all reference summaries
        r_len = len(self.ref_table)
        rb_len = 0
        max_binding = 0
        for r in self.ref_table.itervalues():
            if not r.loc.startswith('/'):
                rb_len += 1
                if len(r.bindings) > max_binding:
                    max_binding = len(r.bindings)

        _log('Total references: {}, with {}({:.0%}) items and ({}) max number of '
             'bindings in the project'.format(r_len, rb_len, 1.0 * rb_len / r_len, max_binding))

        # all summaries about the data whose types were never observed before
        unobserved_count = 0
        unobserved_data = []
        for node, types in self.node2dtypes.iteritems():
            if not set(types).issubset(self.type2attrs):
                unobserved_count += 1
                unobserved_data.append((node, types))
        # with open(join(self.log_dir, 'unseen.txt'), 'w') as f:
        #     for node, types in unobserved_data:
        #         print >>f, str(node), '|'.join(types)

        _log('{}({:.2%}) items have unseen(by static tool) '
             'types!'.format(unobserved_count, (1.0 * unobserved_count) / len(self.node2dtypes)))

        # all summaries to be probabilistic typed
        def longest_cons_len(n, pendings):
            if n.dests:
                to_process = n.dests - n.dests.intersection(pendings)
                max_path = 0
                for d in to_process:
                    pendings.add(d)
                    path = longest_cons_len(d, pendings)
                    pendings.remove(d)
                    if max_path < path:
                        max_path = path
                return max_path + 1
                # path = max(longest_cons_len(d, pendings.add(d)) for d in to_process) + 1
                # pendings = pendings - to_process
            else:
                return 0

        p_len = len(self.nlp_prob_table)
        has_dests = 0
        has_srcs = 0
        max_chain_len = 0
        max_ref_len = 0
        max_bind_len = 0
        binding_len = 0
        ref_len = 0
        has_attrs = 0
        no_src_dest_attr = 0
        for node in self.nlp_prob_table.iterkeys():
            if isinstance(node, Binding):
                binding_len += 1
                m = longest_cons_len(node, {node})
                if max_chain_len < m:
                    max_chain_len = m
                if len(node.refs) > max_ref_len:
                    max_ref_len = len(node.refs)
                if node.dests:
                    has_dests += 1
                if node.srcs:
                    has_srcs += 1
                if not node.srcs and not node.dests:
                    attrs = set()
                    for ref in node.refs:
                        attrs.update(ref.attrs)
                    if not attrs:
                        # print node
                        no_src_dest_attr += 1
                        # pred = self.minor.predicts[node.name][0]
                        # oracle = self.node_types[node]
                        # print (pred in oracle), node.name, node.loc, pred, oracle
            else:
                ref_len += 1
                if len(node.bindings) > max_bind_len:
                    max_bind_len = len(node.bindings)
                if node.attrs:
                    has_attrs += 1

        _log('Total {} items({} bindings, {} references) to be calculated, \n\t'
             '{} with dests, {} with srcs, max constraint chain: {}, '
             'max bindings: {}, {} requiring attrs'.format(p_len, binding_len, ref_len,
                                                           has_dests, has_srcs, max_chain_len,
                                                           max_bind_len, has_attrs))
        _log('Bad: {} bindings has no srcs, dests or even attrs required'.format(no_src_dest_attr))

    def analyze_one(self, gid):
        center = self.binding_table[gid]
        benchmarks = {center}
        benchmarks = benchmarks.union(center.refs.intersection(self.benchmarks))
        self._analyze(benchmarks)
        # make a simple summary

        with open(join(self.log_dir, 'center-' + str(gid) + '-results.txt'), 'w') as f:
            for node in benchmarks:
                node_info = str(node)
                tnames = self.node2dtypes[node]
                sorted_probas = sorted(self.node2predprobas[node].items(), key=lambda x: -x[1])
                dtype_pred_probas = [(tname, self.node2predprobas[node].get(tname, -1)) for tname in tnames]
                dtype_probas_str = ','.join(t + ':' + str(p) for t, p in dtype_pred_probas)
                ptype_probas_str = ','.join(t + ':' + str(p) for t, p in sorted_probas)
                print >>f, '{node} [{dtypes}] [{pred}]'.format(node=node_info,
                                                               dtypes=dtype_probas_str,
                                                               pred=ptype_probas_str)
        with open(join(self.log_dir, 'center-' + str(gid) + '-factors.txt'), 'w') as f2:
            for cluster, pgmodels in self.cluster2pgmodels.iteritems():
                for tname in pgmodels:
                    print >>f2, 'type:', tname
                    for factor in pgmodels[tname].factors:
                        print >>f2, factor
                print >>f2, '*********************************************'

    def analyze(self):
        _log('[Phase I] Merging Static and Dynamic Data')
        self.merge_data()
        _log('[Phase II] Mining NLP Features of Data')
        self.mine_data()
        _log('[Phase III] Preparing Data for Analyzing')
        self.extract_raw_data()
        _log('[Phase IV] Beginning Analyzing')
        self._analyze()

    def _analyze(self, benchmarks=None):
        if benchmarks is None:
            benchmarks = self.benchmarks
        pool = Pool(self.proc_num)
        _log('>>>Clustering benchmarks')
        if self.level >= self.DATAFLOW_LEVEL:
            clusters = self._cluster_benckmarks(benchmarks)
        else:
            clusters = self._cluster_benckmarks(benchmarks, distance=0)
        self.clusters = clusters
        sorted_clusters = sorted(clusters, key=lambda x: -len(x.groups))
        cls_len = len(sorted_clusters)
        _log('>>>Analyzing each cluster...')
        for index, cluster in enumerate(sorted_clusters):
            # _log('[{}/{}] Processing cluster: {}(len={})'.format(index + 1, cls_len,
            #                                                      str(cluster.cid),
            #                                                      len(cluster.groups)))
            print_progress(index + 1, cls_len, suffix='[Processing cluster<%s>(len=%s)]' % (str(cluster.cid), len(cluster.groups)))
            # cluster.draw(self.cluster_dir, benchmarks)
            start = time()

            basic_model = self._construct_basic_model(cluster)
            whole_models = {tname: self._construct_whole_model(basic_model, cluster, tname)
                            for tname in self.type2attrs.keys()}

            # only for debugging
            self.cluster2pgmodels[cluster] = whole_models
            # just for debugging
            if len(cluster.groups) >= self.max_cluster_size:  # or len(cluster.groups) <= 18:
                for group in cluster.focus_groups:
                    cluster.focusgroup2probas[group] = {t: 0.0 for t in self.type2attrs.iterkeys()}

            else:
                # multiprocess-computing the probas for each type
                fgnodes = [self._format_gnode(focus) for focus in cluster.focus_groups]
                to_compute = [(t, fgnodes, m) for t, m in whole_models.iteritems()]
                for tname, marg in pool.map(belief_probas_computer, to_compute):
                    for group in cluster.focus_groups:
                        proba = marg[self._format_gnode(group)].squeeze()[1]
                        if group not in cluster.focusgroup2probas:
                            cluster.focusgroup2probas[group] = {tname: proba}
                        else:
                            cluster.focusgroup2probas[group][tname] = proba

            # record the proba for each node appeared in the benchmarks
            for node, probas in cluster.query_all_focus_nodes():
                if node not in self.node2predprobas:
                    self.node2predprobas[node] = probas
                else:
                    ori_probas = self.node2predprobas[node]
                    for tname, proba in probas.items():
                        if tname not in ori_probas:
                            ori_probas[tname] = proba
                        elif proba > ori_probas[tname]:
                            ori_probas[tname] = proba
            end = time()
            self.cluster2time[cluster] = end - start

    def _cluster_benckmarks(self, benchmarks, distance=3):
        bindings = {n for n in benchmarks if isinstance(n, Binding)}
        refs = {n for n in benchmarks if isinstance(n, Reference)}
        centers = set(bindings)
        for ref in refs:
            for bind in ref.bindings:
                if bind not in centers:
                    centers.add(bind)
        _log('Having {} items to cluster'.format(len(centers)))
        centers_len = len(centers)
        clusters = set()
        for i, center in enumerate(centers):
            # try to collect evidence from backward
            clustered_nodes = self._collect_nodes_for_center(center, distance=distance)
            pruned_cluster = self._prune_collected_nodes(clustered_nodes, center)
            direction = 'backward'
            # if the info from backward is very limited, then try to search forward
            if len(pruned_cluster) == 1 and not self._check_binding_has_type_info(list(pruned_cluster)[0]):
                clustered_nodes = self._collect_nodes_for_center(center, direction='forward', distance=distance)
                pruned_cluster = self._prune_collected_nodes(clustered_nodes, center, direction='forward')
                direction = 'forward'
            new_cluster = self._construct_cluster(self.projroot, benchmarks, pruned_cluster, center, direction)
            clusters.add(new_cluster)
            # print '[{index}/{all}] cluster<{id}> ' \
            #       'has {groups} groups'.format(index=i+1, all=centers_len, id=new_cluster.cid,
            #                                    groups=len(new_cluster.groups))
            print_progress(i + 1, centers_len, suffix='[Cluster<{%s}> has %s groups]' % (new_cluster.cid, len(new_cluster.groups)))

        self._cache_refgroups_to_disk()

        for cls in clusters:
            for focus in cls.focus_nodes:
                if focus not in self.node2clusters:
                    self.node2clusters[focus] = [cls]
                else:
                    self.node2clusters[focus].append(cls)

        # only for debugging
        # _log('Dotting all the clusters...')
        # with open(join(self.log_dir, 'clusters.txt'), 'w') as f:
            # for cls in clusters:
                # print >>f, cls.cid, len(cls.groups), cls.direction, \
                    # ','.join({str(n) for n in cls.node2groups if not n.loc.startswith('/')})
                # cls.draw(self.cluster_dir, benchmarks)

        return clusters

    @staticmethod
    def _check_binding_has_type_info(binding):
        assert isinstance(binding, Binding), 'must be binding'
        for ref in binding.refs:
            if ref.attrs or ref.types or ref.extypes:
                return True
        return False

    @staticmethod
    def _collect_nodes_for_center(center, direction='backward', distance=3):
        processed = []
        worklist = [(center, 0)]
        pending = {center}
        while worklist:
            node, dis = worklist.pop(0)
            pending.remove(node)
            processed.insert(0, node)
            # XXX: Due to the efficiency of proba computation of pgmpy,
            # we only pick 3 srcs(dests).
            if direction == 'backward':
                to_process = list(node.srcs)[:3]
                for binding in to_process:
                    if (binding not in processed) and \
                            (binding not in pending) and (dis < distance):
                        # forget about the src which has no dests and refs,
                        #  and its name is euqal to node.name
                        if binding.srcs or binding.refs or (binding.name != node.name):
                            worklist.append((binding, dis + 1))
                            pending.add(binding)
            else:  # forward
                to_process = list(node.dests)[:3]
                for binding in to_process:
                    if (binding not in processed) and \
                            (binding not in pending) and (dis < distance):
                        # forget about the src which has no dests and refs,
                        #  and its name is euqal to node.name
                        if binding.dests or binding.refs or (binding.name != node.name):
                            worklist.append((binding, dis + 1))
                            pending.add(binding)

            # to_process = list(node.dests)[:3]
            # for binding in to_process:
            #     if (binding not in processed) and \
            #             (binding not in worklist) and (dis < distance):
            #             worklist.append((binding, 3))  # for non-deps, we just add one level

        return processed

    @staticmethod
    def _prune_collected_nodes(clustered_nodes, center, direction='backward'):
        new_clustered_nodes = set(clustered_nodes)
        for node in clustered_nodes:
            if node is center:
                continue
            if direction == 'backward':
                xnodes = node.srcs
            else:
                xnodes = node.dests
            if not xnodes.intersection(new_clustered_nodes):
                if not node.refs:
                    new_clustered_nodes.remove(node)
                else:
                    for ref in node.refs:
                        if ref.attrs or ref.types:
                            break
                    else:
                        new_clustered_nodes.remove(node)
        return new_clustered_nodes

    def _construct_cluster(self, projroot, benchmarks, clustered_nodes, center, direction='backward'):
        # print '++++', str(center)
        # for node in clustered_nodes:
        #     print '>>', str(node)
        # print '-----------------------------'
        new_cluster = set()
        bind2group = {}
        node2groups = {}
        focus_groups = set()
        focus_nodes = set()
        # build all the binds to into groups
        for bind in clustered_nodes:
            bgroup = BindingGroup(bind)
            new_cluster.add(bgroup)
            bind2group[bind] = bgroup
            node2groups[bind] = {bgroup}
            if bind in benchmarks:
                focus_groups.add(bgroup)
                focus_nodes.add(bind)
        # add edges between binding group
        for bind, bgroup in bind2group.iteritems():
            if direction == 'backward':
                srcs, dests = bind.srcs, bind.dests
            else:
                srcs, dests = bind.dests, bind.srcs
            bgroup.srcs.update({bind2group[s] for s in srcs.intersection(clustered_nodes - {bind})})
            bgroup.dests.update({bind2group[s] for s in dests.intersection(clustered_nodes - {bind})})

        # build reference groups and add edges from refgroup to bindgroup
        for bind, bgroup in bind2group.iteritems():
            if bind in self.binding2refgroups:
                ref_groups = self.binding2refgroups[bind]
            else:
                if self.async_refcluster:
                    ref_groups = [refs for refs in async_cluster_refs(projroot, bind)]
                else:
                    ref_groups = [refs for refs in cluster_refs(projroot, bind)]
                self.binding2refgroups[bind] = ref_groups

            # index all the ref groups
            features2refgroup = {}
            refs_ready_to_target_bgroup = set()  # refs to be bound with current bind group
            for refs in ref_groups:
                rgroup = ReferenceGroup(*refs)
                # we ignore refgroup without any syntactic infomation
                if rgroup.attrs or rgroup.types or rgroup.extypes:
                    hash_code = self._hash_refgroup(rgroup)
                    # Due to efficency, we merge the groups with the same features
                    if hash_code not in features2refgroup:
                        features2refgroup[hash_code] = rgroup
                    else:
                        features2refgroup[hash_code].add_elts(*rgroup.elts)
                else:
                    # try: when finding a path has no any operation, we add None to its type set
                    # bgroup.types.add('None')
                    refs_ready_to_target_bgroup.update(refs)

            # Due to the efficiency of pgm, we restrict the refgroups into no more than 10
            sorted_refgroups = sorted(features2refgroup.values(), key=lambda x: -len(x.elts))
            selected_ref_groups = sorted_refgroups[:10]
            for rgroup in selected_ref_groups:
                bgroup.rgroups.add(rgroup)
                rgroup.bgroups.add(bgroup)
                new_cluster.add(rgroup)

            # for all the ignored_ref_groups, we will bind the refs to the binding group
            ignored_ref_groups = sorted_refgroups[10:]
            for igroup in ignored_ref_groups:
                refs_ready_to_target_bgroup.update(igroup.elts)

            # target selected ref groups
            for rgroup in selected_ref_groups:
                for ref in rgroup.elts:
                    if ref not in node2groups:
                        node2groups[ref] = {rgroup}
                    else:
                        node2groups[ref].add(rgroup)
                    if ref in benchmarks:
                        focus_groups.add(rgroup)
                        focus_nodes.add(ref)

            # target ignored ref groups, including non-featured groups and ignored groups
            for ref in refs_ready_to_target_bgroup:
                if ref not in node2groups:
                    node2groups[ref] = {bgroup}
                else:
                    node2groups[ref].add(bgroup)
                if ref in benchmarks:
                    focus_groups.add(bgroup)
                    focus_nodes.add(ref)

            # target rest refs that have not been handled
            for ref in bind.refs:
                if ref not in node2groups:
                    node2groups[ref] = {bgroup}
                    if ref in benchmarks:
                        focus_groups.add(bgroup)
                        focus_nodes.add(ref)

            # features2refgroup = {}
            # for refs in ref_groups:
            #     rgroup = ReferenceGroup(*refs)
            #     # we ignore refgroup without any syntactic infomation
            #     if rgroup.attrs or rgroup.types or rgroup.extypes:
            #         hash_code = self._hash_refgroup(rgroup)
            #         # Due to efficency, we merge the groups with the same features
            #         if hash_code in features2refgroup:
            #             # the target_group is used to link a focus node
            #             target_group = features2refgroup[hash_code]
            #             target_group.add_elts(*rgroup.elts)
            #         else:
            #             target_group = rgroup
            #             bgroup.rgroups.add(rgroup)
            #             rgroup.bgroups.add(bgroup)
            #             new_cluster.add(rgroup)
            #             features2refgroup[hash_code] = rgroup
            #     else:
            #         # if the rgroup is ignored, we map the ref to the groups of its binds
            #         target_group = bgroup
            #     # map each ref to the groups it belongs to
            #     for ref in refs:
            #         if ref not in node2groups:
            #             node2groups[ref] = {target_group}
            #         else:
            #             node2groups[ref].add(target_group)
            #         if ref in benchmarks:
            #             focus_groups.add(target_group)
            #             focus_nodes.add(ref)

        return Cluster(center, new_cluster, node2groups, focus_groups, focus_nodes, direction)

    @staticmethod
    def _hash_refgroup(refgroup):
        return ','.join(['|'.join(refgroup.attrs),
                         '|'.join(refgroup.types),
                         '|'.join(refgroup.extypes)])

    def _try_load_disk_bind2refs_cache(self):
        try:
            with open(join('tmp', self.projname + '-refcache.txt')) as f:
                for line in filter(bool, f.read().split('\n')):
                    data = filter(bool, re.findall(r'<(.*?)>', line))
                    binding = self.binding_table[int(data[0])]
                    refs = {{self.ref_table[int(r)] for r in map(int, d.split('|'))} for d in data[1:]}
                    self.binding2refgroups[binding] = refs
        except IOError:
            pass

    def _cache_refgroups_to_disk(self):
        try:
            with open(join('tmp', self.projname + '-refcache.txt'), 'w') as f:
                for binding, ref_groups in self.binding2refgroups.iteritems():
                    binfo = '<' + str(binding.gid) + '> '
                    rinfo = ' '.join('<'+'|'.join(str(r.gid) for r in refs) + '>' for refs in ref_groups)
                    print >>f, binfo, rinfo
        except IOError:
            pass

    def _construct_basic_model(self, cluster, model='markov', check_model=False):
        if model == 'markov':
            graph = MarkovModel(checked=(not check_model))
        elif model == 'factor':
            graph = FactorGraph(checked=(not check_model))
        else:
            raise ValueError('Unknown model type.')
        # add random variables
        graph.add_nodes_from([self._format_gnode(node) for node in cluster.groups])
        for dest in cluster.binding_groups:
            # build connection factors between reference group
            if dest.rgroups:
                self._build_connection_factors(dest, dest.rgroups, graph)
            # build connection factors between binding groups
            if dest.srcs:
                self._build_connection_factors(dest, dest.srcs, graph)
        return graph

    def _build_nlp_factor(self, dest, graph):
        gdest_node = self._format_gnode(dest)
        gsrc_node = self._format_gnode(dest, 'nlp-')
        graph.add_node(gsrc_node)
        eta = self.naming_belief_proba
        factor_nlp2broup = Factor([gsrc_node, gdest_node], [2, 2], [eta, eta, 1 - eta, eta])
        # factor_bgroup2nlp = Factor([gdest_node, gsrc_node], [2, 2], [0.7, 0.7, 0.3, 0.7])
        graph.add_factors(factor_nlp2broup)
        # graph.add_factors(factor_bgroup2nlp)
        if isinstance(graph, FactorGraph):
            graph.add_node(factor_nlp2broup)
            # graph.add_node(factor_bgroup2nlp)
            graph.add_edges_from([(gsrc_node, factor_nlp2broup), (gdest_node, factor_nlp2broup)])
            # graph.add_edges_from([(gsrc_node, factor_bgroup2nlp), (gdest_node, factor_bgroup2nlp)])
        else:
            graph.add_edges_from([(gsrc_node, gdest_node)])

    def _add_nlp_primitive_factor(self, bgroup, tname, graph):
        gnode = self._format_gnode(bgroup, 'nlp-')
        xproba = self._get_dm_proba(bgroup.name, tname)
        proba = [1 - xproba, xproba]
        factor_node = Factor([gnode], [2], proba)
        graph.add_factors(factor_node)
        if isinstance(graph, FactorGraph):
            graph.add_edge(gnode, factor_node)

    def _build_connection_factors(self, dest, srcs, graph):
        gdest_node = self._format_gnode(dest)
        gsrc_nodes = [self._format_gnode(src) for src in srcs]
        # srcs -> dest,  s1 -> d, s2 -> d, ...
        # for gsrc_node in gsrc_nodes:
        #     factor_src2dest = Factor([gsrc_node, gdest_node], [2, 2],
        #                              [1.0, 1.0, 0.0, 1.0])
        #     graph.add_factors(factor_src2dest)
        #     if isinstance(graph, FactorGraph):
        #         graph.add_node(factor_src2dest)
        #         graph.add_edges_from([(gsrc_node, factor_src2dest),
        #                               (gdest_node, factor_src2dest)])
        #     else:
        #         graph.add_edge(gsrc_node, gdest_node)

        # srcs -> dest, s1 V s2 V s3 V... -> dest
        n = 2 ** (len(srcs) + 1)
        # len([1, 1, 0, 1, 0, 1, ...]) = 2^n
        srcs2dest = np.array([1] + [0 if i % 2 else 1 for i in xrange(n-1)])
        factor_srcs2dest = Factor(gsrc_nodes + [gdest_node], [2] * (1 + len(srcs)), srcs2dest)
        graph.add_factors(factor_srcs2dest)
        if isinstance(graph, FactorGraph):
            graph.add_node(factor_srcs2dest)
            graph.add_edges_from([(gsrc, factor_srcs2dest) for gsrc in gsrc_nodes])
            graph.add_edge(gdest_node, factor_srcs2dest)
        else:
            graph.add_edges_from(combinations([gdest_node] + gsrc_nodes, 2))

        # dest -> s1, dest -> s2
        # for gsrc_node in gsrc_nodes:
        #     factor_dest2src = Factor([gdest_node, gsrc_node], [2, 2],
        #                              [1.0, 1.0, 0.0, 1.0])
        #     graph.add_factors(factor_dest2src)
        #     if isinstance(graph, FactorGraph):
        #         graph.add_node(factor_dest2src)
        #         graph.add_edges_from([(gsrc_node, factor_dest2src),
        #                               (gdest_node, factor_dest2src)])
        #     else:
        #         graph.add_edge(gsrc_node, gdest_node)

        # dest -> srcs,  d -> s1 V s2 V s3 V ...
        n = 2 ** len(srcs)
        dest2srcs = np.concatenate([np.ones(n), np.zeros(1), np.ones(n - 1)])
        factor_dest2srcs = Factor([self._format_gnode(node) for node in ([dest] + list(srcs))],
                                  [2] * (1 + len(srcs)), dest2srcs)
        graph.add_factors(factor_dest2srcs)
        if isinstance(graph, FactorGraph):
            graph.add_node(factor_dest2srcs)
            graph.add_edges_from([(gsrc, factor_dest2srcs) for gsrc in gsrc_nodes])
            graph.add_edge(gdest_node, factor_dest2srcs)
        else:
            graph.add_edges_from(combinations([gdest_node] + gsrc_nodes, 2))

    def _construct_whole_model(self, basic_model, cluster, tname):
        graph = deepcopy(basic_model)
        # add primitive factors for nlp node of the center
        # for node in cluster.binding_groups:
        #     self._add_nlp_primitive_factor(node, tname, graph)
        if self.level >= self.NAMING_LEVEL:
            self._build_nlp_factor(cluster.center_group, graph)
            self._add_nlp_primitive_factor(cluster.center_group, tname, graph)

        # add primitive factors
        for node in cluster.groups:
            gnode = self._format_gnode(node)
            xproba = -1
            if node.extypes and (tname in node.extypes):
                xproba = 0.1
            elif node.attrs:
                if node.attrs.issubset(self.type2attrs[tname]):
                    if node.types and (tname in node.types):
                        xproba = self._gen_proba_bi_or_nonbi(tname, 0.92)

                    else:
                        xproba = self._gen_propa_for_subset(node.attrs, tname)
                else:
                    if node.types and (tname in node.types):
                        xproba = self._gen_proba_bi_or_nonbi(tname, 0.90)
                    else:
                        xproba = self._gen_proba_for_non_subset(node.attrs, tname)
            else:
                if node.types and (tname in node.types):
                    xproba = self._gen_proba_bi_or_nonbi(tname, 0.90)
                elif isinstance(node, ReferenceGroup):
                    xproba = (self._get_dm_proba(node.name, tname) + 0.5) / 2
            # an emperical heuristics
            if xproba != -1:
                xproba = self._gen_proba_base_or_nonbase(tname, xproba)
            # if node.types and (tname in node.types):
            #     xproba = 0.9
            # elif node.extypes and (tname in node.extypes):
            #     xproba = 0.1
            # elif node.attrs:
            #     if node.attrs.issubset(self.type_table[tname]):
            #         xproba = self._gen_propa_for_subset(node.attrs, tname)
            #     else:
            #         xproba = self._gen_proba_for_non_subset(node.attrs, tname)
            # elif isinstance(node, ReferenceGroup):
            #     xproba = (self._get_dm_proba(node.name, tname) + 0.5) / 2
            if xproba != -1:
                proba = [1 - xproba, xproba]
                factor_node = Factor([gnode], [2], proba)
                graph.add_factors(factor_node)
                if isinstance(graph, FactorGraph):
                    graph.add_edge(gnode, factor_node)
        return graph

    def _get_dm_proba(self, name, tname):
        probas = self.minor.predict_proba(name)
        xproba = probas.get(tname, 0.0)
        return xproba if xproba >= 0.0001 else 0.0001

    @staticmethod
    def _gen_proba_bi_or_nonbi(tname, base_proba=0.9):
        return (base_proba + 0.02) if tname in bitypes.types else base_proba

    def _gen_proba_base_or_nonbase(self, tname, base_proba=0.9):
        return (base_proba + 0.01) if tname in self.type2subtypes else base_proba

    def _compute_uniqueness_proba(self, u, attrs):
        if not attrs: return 0.5
        return 0.5 + 0.5 * (2 * u - 1) / sqrt(self._compute_uniqueness(attrs))

    def _compute_uniqueness(self, attrs):
        attrs_tuple = tuple(attrs)
        if attrs_tuple in self.attrs2unique:
            return self.attrs2unique[attrs_tuple]
        n = sum((1 if set(attrs).issubset(allattrs) else 0) for allattrs in self.type2attrs.itervalues())
        self.attrs2unique[attrs_tuple] = n
        return n

    def _gen_propa_for_subset(self, attrs, tname):
        # if all(1 if a.startswith('__') and a.endswith('__') else 0 for a in attrs):
            # if tname in bitypes.types:
                # return 0.9
            # else:
                # return 0.8
        # if tname in bitypes.types:
        #     return 0.95
        # if len(attrs) == 1 and {'__iter__', '__getitem__'}.intersection(attrs):
        #     return 0.5
        base = self._compute_uniqueness_proba(self.high_proba, attrs)
        if all(1 if a.startswith('__') and a.endswith('__') else 0 for a in attrs):
            return self._gen_proba_bi_or_nonbi(tname, base)
        return self._gen_proba_bi_or_nonbi(tname, base + 0.02)

    def _gen_proba_for_non_subset(self, attrs, tname):
        co_attrs = attrs.intersection(self.type2attrs[tname])
        xproba = (1.0 * len(co_attrs) / len(attrs)) * 0.5
        return xproba

    def make_analyze_summary(self):

        def _format_float(x):
            return float('%.4f' % x)

        # -----------------------------------------
        # summary
        benchmarks = self.benchmarks
        _log('>>>Making summaries...')
        _log('---------------analysis results---------------')
        # extract gap
        thresholds = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
        gaps = [0.05, 0.1, 0.15, 0.2]#, 0.25, 0.30]
        count_all = len(benchmarks)
        count_max_proba_right = 0
        count_max_three_right_in = 0
         # {0.1:{node1:[(t1, p1), (t2, p2), ...], node2:[...]}, 0.2:{...}}
        threshold2node_type_probas = {t: {} for t in thresholds}
        gap2node_type_probas = {t: {} for t in gaps}
        # collect each types over each threshold for each node
        positives = 0
        for node in benchmarks:
            tnames = self.node2dtypes[node]
            if node in self.node2predprobas:
                sorted_probas = sorted(self.node2predprobas[node].items(), key=lambda x: -x[1])
            else:
                sorted_probas = {tname: 0.0 for tname in self.type2attrs.iterkeys()}.items()
                _log('Warning: the proba of node: {} is missing'.format(str(node)))
            for threshold in thresholds:
                probas_over_threshold = filter(lambda x: x[1] >= threshold, sorted_probas)
                if probas_over_threshold:
                    threshold2node_type_probas[threshold][node] = probas_over_threshold
            if sorted_probas[0][0] in tnames:
                count_max_proba_right += 1
            if [tname for tname, proba in sorted_probas[:3] if tname in tnames]:
                count_max_three_right_in += 1
            if sorted_probas[0][1] > 0.5:
                positives += 1
            # --------------------------------
            # gaps
            # for gap in gaps:
            #     max_proba = sorted_probas[0][1]
            #     min_proba = 0.5 if max_proba - gap < 0.5 else max_proba - gap
            #     first_cluster = filter(lambda x: min_proba <= x[1] <= max_proba, sorted_probas)
            #     if first_cluster:
            #         gap2node_type_probas[gap][node] = first_cluster
            for gap in gaps:
                groups = self._group_data_by_gap(sorted_probas, gap)
                first_cluster = filter(lambda x: x[1] > 0.5, groups[0])
                if first_cluster:
                    gap2node_type_probas[gap][node] = first_cluster

        # _log('Accuracy(max proba): {num}({per:.2%})'.format(num=count_max_proba_right,
        #                                                     per=1.0 * count_max_proba_right / count_all))
        # _log('Accuracy(top 3 probas): {num}({per:.2%})'.format(num=count_max_three_right_in,
        #                                                        per=1.0 * count_max_three_right_in / count_all))
        self.two_naive_accuracies = (1.0 * count_max_proba_right / count_all, 1.0 * count_max_three_right_in / count_all)

        # self._make_threshold_summary(thresholds, threshold2node_type_probas)

        # _log('Summarizing Gaps...')
        _log('Nodes: ' + str(len(benchmarks)))
        _log('Positives: {:.2%}'.format(1.0 * positives/len(benchmarks)))
        self._make_gap_summary(gaps, gap2node_type_probas)

        max_ctime = max(self.cluster2time.itervalues())
        avg_ctime = sum(self.cluster2time.itervalues())/len(self.cluster2time)
        min_ctime = min(self.cluster2time.itervalues())
        # _log('Analyzing time(each cluster)(sec): '
        #      'max[{}], avg[{}], min[{}]'.format(_format_float(max_ctime), _format_float(avg_ctime), _format_float(min_ctime)))

        focuses_time = zip([c.focus_nodes for c in self.clusters], self.cluster2time.values())
        max_ntime = max(t/len(f) for f, t in focuses_time)
        avg_ntime = sum(self.cluster2time.itervalues())/len(benchmarks)
        min_ntime = min(t/len(f) for f, t in focuses_time)
        self.time_matrix = (max_ctime, avg_ctime, min_ctime, max_ntime, avg_ntime, min_ntime)
        # _log('Analyzing time(each node)(sec): '
        #     'max[{}], avg[{}], min[{}]'.format(_format_float(max_ntime), _format_float(avg_ntime), _format_float(min_ntime)))
        _log('Avg. analyzing time(each node)(sec): {}'.format(_format_float(avg_ntime)))
        
    @staticmethod
    def _group_data_by_gap(type_probas, gap):
        elm_wise_data = zip(type_probas, type_probas[1:])
        groups = []
        current_group = [type_probas[0]]
        for (t1, p1), (t2, p2) in elm_wise_data:
            if p1 - p2 <= gap:
                current_group.append((t2, p2))
            else:
                groups.append(current_group)
                current_group = []
        if current_group:
            groups.append(current_group)
        return groups

    def _make_threshold_summary(self, thresholds, threshold2node_type_probas):
        benchmarks = self.benchmarks
        count_all = len(benchmarks)
        faults = {}  # threshold -> fault item
        longtsets = {}  # threshold -> nodes of tset size over 5
        faults_out_of_top5 = {}
        # summary for each threshold
        for threshold in thresholds:
            max_type_set_len = 0
            min_type_set_len = 10000
            sum_type_set_len = 0
            sum_type_set_len_max5 = 0
            count_tset_len_ge_3 = 0
            count_tset_len_ge_4 = 0
            count_tset_len_ge_5 = 0
            count_over_threshold = len(threshold2node_type_probas[threshold])
            count_over_threshold_include = 0
            count_top5_over_threshold_include = 0
            count_over_threshold_max_hit = 0
            count_over_threshold_has_overlap = 0
            faults[threshold] = {}
            faults_out_of_top5[threshold] = []
            longtsets[threshold] = []
            for node, type_probas in threshold2node_type_probas[threshold].iteritems():
                probas_len = len(type_probas)
                if probas_len > max_type_set_len:
                    max_type_set_len = probas_len
                if probas_len < min_type_set_len:
                    min_type_set_len = probas_len
                sum_type_set_len += probas_len
                sum_type_set_len_max5 += (5 if probas_len > 5 else probas_len)
                if probas_len > 3:
                    count_tset_len_ge_3 += 1
                if probas_len > 4:
                    count_tset_len_ge_4 += 1
                if probas_len > 5:
                    count_tset_len_ge_5 += 1

                tset, pred_tset = set(self.node2dtypes[node]), set(dict(type_probas).iterkeys())
                top5_pred_tset = set(dict(type_probas[:5]).iterkeys())

                # merge the statical inferred types
                if node in self.node2stypes:
                    pred_tset = pred_tset.union(self.node2stypes[node])
                    top5_pred_tset = top5_pred_tset.union(self.node2stypes[node])

                if probas_len >= 5:
                    longtsets[threshold].append((node, tset, type_probas))

                if tset.issubset(pred_tset):
                    count_over_threshold_include += 1
                else:
                    faults[threshold][node] = (tset, type_probas)

                if len(tset) > 5 and tset.issubset(pred_tset):
                    count_top5_over_threshold_include += 1
                elif tset.issubset(top5_pred_tset):
                    count_top5_over_threshold_include += 1
                else:
                    faults_out_of_top5[threshold].append((node, tset, type_probas))

                if type_probas and type_probas[0][0] in tset:
                    count_over_threshold_max_hit += 1
                if tset.intersection(pred_tset):
                    count_over_threshold_has_overlap += 1
            _log('-----------')
            _log('Threshold: [' + str(threshold) + ']')

            max_count = sum(1 for p in threshold2node_type_probas[threshold].itervalues()
                            if len(p) == max_type_set_len)
            min_count = sum(1 for p in threshold2node_type_probas[threshold].itervalues()
                            if len(p) == min_type_set_len)

            _log('Type set size: max[{}], avg1[{}], '
                 'avg2[{}], avg3[{}], min[{}]'.format(max_type_set_len,
                                                      sum_type_set_len/count_over_threshold,
                                                      (sum_type_set_len - max_count * max_type_set_len) /
                                                        (count_over_threshold - max_count),
                                                      sum_type_set_len_max5/count_over_threshold,
                                                      min_type_set_len))
            _log('Node count: (len>3)[{}], (len>4)[{}], (len>5)[{}]'.format(count_tset_len_ge_3,
                                                                            count_tset_len_ge_4,
                                                                            count_tset_len_ge_5))
            _log('Accuracy[pred-types superset dyn-types]: '
                 '{num}({per:.2%})'.format(thre=threshold, num=count_over_threshold_include,
                                           per=1.0*count_over_threshold_include/count_over_threshold))
            _log('Accuracy[pred-types(top 5) superset dyn-types]: '
                 '{num}({per:.2%})'.format(thre=threshold, num=count_top5_over_threshold_include,
                                           per=1.0*count_top5_over_threshold_include/count_over_threshold))

            _log('Accuracy[pred-max-type included in dyn-types]: '
                 '{num}({per:.2%})'.format(thre=threshold, num=count_over_threshold_max_hit,
                                           per=1.0*count_over_threshold_max_hit/count_over_threshold))
            _log('Accuracy[pred-types intersect dyn-types]: '
                 '{num}({per:.2%})'.format(thre=threshold, num=count_over_threshold_has_overlap,
                                           per=1.0*count_over_threshold_has_overlap/count_over_threshold))

            _log('Recall: {per:.2%}'.format(thre=threshold, per=1.0*count_over_threshold/count_all))
            self.thre2matrix[threshold] = {'total-benchmarks': len(benchmarks),
                                           'tset-max': max_type_set_len, 'tset-avg1': sum_type_set_len/count_over_threshold,
                                           'tset-avg2': (sum_type_set_len - max_count * max_type_set_len) /
                                                        (count_over_threshold - max_count),
                                           'tset-avg3': sum_type_set_len_max5/count_over_threshold,
                                           'tset-min': min_type_set_len, 'ncount-ge-3': count_tset_len_ge_3,
                                           'ncount-ge-4': count_tset_len_ge_4, 'ncount-ge-5': count_tset_len_ge_5,
                                           'percent-ge-3': 1.0*count_tset_len_ge_3/len(benchmarks),
                                           'percent-ge-4': 1.0*count_tset_len_ge_4/len(benchmarks),
                                           'percent-ge-5': 1.0*count_tset_len_ge_5/len(benchmarks),
                                           'accuracy-superset': 1.0*count_over_threshold_include/count_over_threshold,
                                           'accuracy-t5-superset': 1.0*count_top5_over_threshold_include/count_over_threshold,
                                           'accuracy-maxin': 1.0*count_over_threshold_max_hit/count_over_threshold,
                                           'accuracy-intersect': 1.0*count_over_threshold_has_overlap/count_over_threshold,
                                           'recall': 1.0*count_over_threshold/count_all}



        _log('-----------')

        _log('Dumping results to the log file')

        # with open(join(self.log_dir, 'results.txt'), 'w') as f:
        #     with open(join(self.log_dir, 'faults.txt'), 'w') as f2:
        #         for node in benchmarks:
        #             node_info = str(node)
        #             tnames = self.node_types[node]
        #             tstrs = '|'.join(tnames)
        #             pred_probas = [(tname, self.node_pred_probas[node].get(tname, -1)) for tname in tnames]
        #             sorted_probas = sorted(self.node_pred_probas[node].items(), key=lambda x: -x[1])
        #             type_probas = ','.join(t+':'+str(p) for t, p in sorted_probas)
        #             print >>f, '{node} [{dtypes}] [{pred}]'.format(node=node_info, dtypes=tstrs, pred=type_probas)
        #             if sorted_probas[0][0] not in tnames:
        #                 dtype_probas = ','.join(t + ':' + str(p) for t, p in pred_probas)
        #                 print >>f2, '{node} [{dtypes}] [{pred}]'.format(node=node_info,
        #                                                                 dtypes=dtype_probas, pred=type_probas)

        fault_cluster2types = {}
        with open(join(self.log_dir, 'faults-sep.txt'), 'w') as f3:
            for threshold, records in sorted(faults_out_of_top5.items(), key=lambda x: -x[0]):
                print >>f3, 'Threshold: ' + str(threshold)
                # key=lambda x: x[0].loc + '@' + x[0].name
                sort_key = lambda x: '|'.join(str(c.cid) for c in self.node2clusters[x[0]])
                for node, tset, type_probas in sorted(records, key=sort_key):
                    dtype_pred_probas = [(tname, self.node2predprobas.get(node, {}).get(tname, -1)) for tname in tset]
                    dtype_probas_str = ','.join(t + ':' + str(p) for t, p in dtype_pred_probas)
                    ptype_probas_str = ','.join(t + ':' + str(p) for t, p in type_probas)
                    for cls in self.node2clusters[node]:
                        if cls not in fault_cluster2types:
                            fault_cluster2types[cls] = set()
                        cared_set = tset.union(set(dict(type_probas).keys())) - \
                                    tset.intersection(set(dict(type_probas).keys()))
                        fault_cluster2types[cls].update(cared_set)
                    print >>f3, '{infaults}[{cid}] {node} [{dtypes}] ' \
                                '[{pred}]'.format(infaults='' if node in faults[threshold] else '*',
                                                  cid='|'.join(str(c.cid) for c in self.node2clusters[node]),
                                                  node=str(node), dtypes=dtype_probas_str, pred=ptype_probas_str)
                print >>f3, '--------------------------------------------'

        # with open(join(self.log_dir, 'faults-cls.txt'), 'w') as f4:
        #     for cluster, types in fault_cluster2types.iteritems():
        #         print >>f4, 'cluster:', str(cluster)
        #         if cluster not in self.cluster2pgmodels:
        #             continue
        #         pgmodels = self.cluster2pgmodels[cluster]
        #         for tname in types:
        #             print >>f4, 'type:', tname
        #             if tname not in pgmodels:
        #                 continue
        #             for factor in pgmodels[tname].factors:
        #                 print >>f4, factor
        #         print >>f4, '*********************************************'

        # for cluster, types in fault_cluster2types.iteritems():
            # with open(join(self.cluster_dir, 'tcons-' + str(cluster.cid) + '-factors.txt'), 'w') as f4:
                # print >>f4, 'cluster:', str(cluster)
                # if cluster not in self.cluster2pgmodels:
                    # continue
                # pgmodels = self.cluster2pgmodels[cluster]
                # for tname in types:
                    # print >>f4, 'type:', tname
                    # if tname not in pgmodels:
                        # continue
                    # for factor in pgmodels[tname].factors:
                        # print >>f4, factor
                # print >>f4, '*********************************************'

        _log('Done!')

        with open(join(self.log_dir, 'longtsets-sep.txt'), 'w') as f4:
            for threshold, records in sorted(longtsets.items(), key=lambda x: -x[0]):
                print >> f4, 'Threshold: ' + str(threshold)
                for node, tset, type_probas in sorted(records, key=lambda x: -len(x[2])):
                    dtype_pred_probas = [(tname, self.node2predprobas.get(node, {}).get(tname, -1)) for tname in tset]
                    dtype_probas_str = ','.join(t + ':' + str(p) for t, p in dtype_pred_probas)
                    ptype_probas_str = ','.join(t + ':' + str(p) for t, p in type_probas)
                    print >>f4, '[{len}] [{cid}] {node} [{dtypes}] ' \
                                '[{pred}]'.format(cid='|'.join(str(c.cid) for c in self.node2clusters[node]),
                                                  node=str(node), dtypes=dtype_probas_str,
                                                  len=len(type_probas), pred=ptype_probas_str)
                print >>f4, '--------------------------------------------'

    def _make_gap_summary(self, gaps, gap2node_type_probas):

        def _format_float(x):
            return float('%.4f' % x)

        benchmarks = self.benchmarks
        count_all = len(benchmarks)
        # summary for each gap
        tops = [3, 5, 7]
        for gap in gaps:
            self.gap2matrix[gap] = {}
            count_first_class = len(gap2node_type_probas[gap])
            for top_n in tops:
                topn_sum_type_set_len = 0
                count_first_class_topn_include = 0
                count_first_class_topn_recall = 0
                for node, type_probas in gap2node_type_probas[gap].iteritems():
                    probas_len = len(type_probas)
                    topn_sum_type_set_len += (top_n if probas_len > top_n else probas_len)
                    tset = set(self.node2dtypes[node])
                    topn_pred_tset = set(dict(type_probas[:top_n]).iterkeys())
                    # merge the statical inferred types
                    if node in self.node2stypes:
                        topn_pred_tset = topn_pred_tset.union(self.node2stypes[node])

                    if len(tset) > top_n and topn_pred_tset.issubset(tset):
                        count_first_class_topn_include += 1
                        count_first_class_topn_recall += 1
                    elif tset.issubset(topn_pred_tset):
                        count_first_class_topn_include += 1
                        count_first_class_topn_recall += 1
                    else:
                        recall = 1.0 * len(tset.intersection(topn_pred_tset)) / len(tset)
                        count_first_class_topn_recall += recall
                self.gap2matrix[gap][top_n] = {'total-benchmarks': len(benchmarks),
                                               'topn-tset-avg-size': topn_sum_type_set_len/count_first_class,
                                               'topn-include-percent': 1.0*count_first_class_topn_include / count_first_class,
                                               'topn-recall-all': 1.0 * count_first_class_topn_recall / count_all,
                                               'topn-recall-exclude-negative': 1.0 * count_first_class_topn_recall / count_first_class
                                               }
            
            _log('Gap: [' + str(gap) + ']')
            _log('Recall: ' + ','.join('[Top{}]:{}'.format(top_i, _format_float(mat['topn-recall-all']))
                                       for top_i, mat in self.gap2matrix[gap].iteritems()))
            _log('Avg. Type Set Size: ' + ', '.join('[Top{}]:{}'.format(top_i, mat['topn-tset-avg-size'])
                                                    for top_i, mat in self.gap2matrix[gap].iteritems()))
            _log('-----------')
        _log('>>>Dumping results to the log file')
        max_top_n = max(tops)
        node2types_number = {}  # node -> [type# of gap1, type# of gap2, ...]
        for gap in gaps:
            for node, type_probas in gap2node_type_probas[gap].iteritems():
                count_first_class = len(type_probas)
                if node not in node2types_number:
                    node2types_number[node] = []
                node2types_number[node].append(max_top_n if count_first_class > max_top_n else count_first_class)
        results = []
        for node in benchmarks:
            tnames = self.node2dtypes[node]
            if node in self.node2predprobas:
                sorted_probas = sorted(self.node2predprobas[node].items(), key=lambda x: -x[1])
            else:
                sorted_probas = {tname: 0.0 for tname in self.type2attrs.iterkeys()}.items()
            max_cluster = node2types_number[node][-1] if node in node2types_number else 0
            results.append([node, [(tname, self.node2predprobas[node].get(tname, -1)) for tname in tnames],
                            node2types_number[node] if node in node2types_number else [],
                            sorted_probas[:max_cluster], sorted_probas[max_cluster:]])

        with open(join(self.log_dir, 'analysis-results-'+ str(self.level) + '-' + str(int(self.high_proba*100))+ '-' + str(int(self.naming_belief_proba*100))+'.txt'), 'w') as f:
            sort_key = lambda x: '|'.join(str(c.cid) for c in self.node2clusters[x[0]])
            sorted_results = sorted(results, key=sort_key)
            for node, dtype_probas, gap_types_number, cared_type_probas, rest_type_probas in sorted_results:
                dtype_probas_str = ','.join(t + ':' + str(p) for t, p in dtype_probas)
                gap_types_number_str = ','.join(str(x) for x in gap_types_number)
                cared_type_probas_str = ','.join(t + ':' + str(p) for t, p in cared_type_probas)
                rest_type_probas_str = ','.join(str(t) + ':' + str(p) for t, p in rest_type_probas)
                print >>f, '[{cid}] {node} [{gaps}][{cared}] ' \
                           '[{dtypes}][{rest}]'.format(cid='|'.join(str(c.cid) for c in self.node2clusters[node]),
                                                       node=str(node), gaps=gap_types_number_str,
                                                       cared=cared_type_probas_str, dtypes=dtype_probas_str,
                                                       rest=rest_type_probas_str)
        _log('Done!')


    @staticmethod
    def _format_gnode(node, prefix=''):
        if isinstance(node, Reference):
            return prefix + 'R' + str(node.gid)
        elif isinstance(node, Binding):
            return prefix + 'B' + str(node.gid)
        elif isinstance(node, ReferenceGroup):
            return prefix + 'RC' + str(node.cid)
        else:
            return prefix + 'BC' + str(node.cid)

    @staticmethod
    def map_astop_mthd(op):
        if isinstance(op, Add):
            return '__add__'
        elif isinstance(op, Sub):
            return '__sub__'
        elif isinstance(op, Mult):
            return '__mul__'
        elif isinstance(op, Div):
            return '__div__'
        elif isinstance(op, Mod):
            return '__mod__'
        elif isinstance(op, Pow):
            return '__pow__'
        elif isinstance(op, LShift):
            return '__lshift__'
        elif isinstance(op, RShift):
            return '__rshift__'
        elif isinstance(op, BitOr):
            return '__or__'
        elif isinstance(op, BitAnd):
            return '__and__'
        elif isinstance(op, FloorDiv):
            return '__floordiv__'
        elif isinstance(op, Invert):
            return '__inver__'

